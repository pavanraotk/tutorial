package com.rakuten.online.test.persistence;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.rakuten.online.test.commons.util.DatabaseConstants;

/**
 * The persistent class for the test database table.
 * 
 */
@Entity
@Table(name = DatabaseConstants.TEST_TABLE)
@NamedQueries({
        @NamedQuery(name = DatabaseConstants.FIND_ALL_TESTS, query = "SELECT t FROM Test t"),
        @NamedQuery(name = DatabaseConstants.FIND_ALL_TESTS_BY_CATEGORY, query = "SELECT t FROM Test t WHERE t.category =:category"),
        @NamedQuery(name = DatabaseConstants.FIND_ALL_TESTS_BY_EXPERIENCE, query = "SELECT t FROM Test t WHERE t.experience =:experience") })
public class Test implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = DatabaseConstants.TEST_ID)
    private int testId;

    @Column(name = DatabaseConstants.CATEGORY)
    private int category;

    @Column(name = DatabaseConstants.DURATION)
    private int duration;

    @Column(name = DatabaseConstants.EXPERIENCE)
    private BigDecimal experience;

    // bi-directional many-to-many association to Question
    @ManyToMany(cascade={CascadeType.ALL})
    @JoinTable(name = DatabaseConstants.TESTQUESTIONS, joinColumns = { @JoinColumn(name = DatabaseConstants.TESTID) }, inverseJoinColumns = { @JoinColumn(name = DatabaseConstants.QUESTIONID) })
    private transient List<Question> questions;

    public Test() {
        Random random = new Random();
        this.testId = random.nextInt(10000);
    }

    public int getTestId() {
        return this.testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public int getCategory() {
        return this.category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getDuration() {
        return this.duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public BigDecimal getExperience() {
        return this.experience;
    }

    public void setExperience(BigDecimal experience) {
        this.experience = experience;
    }

    public List<Question> getQuestions() {
        return this.questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        return "Test [testId=" + testId + ", category=" + category
                + ", duration=" + duration + ", experience=" + experience
                + ", questions=" + questions + "]";
    }
}