package com.rakuten.online.test.persistence;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.rakuten.online.test.commons.util.DatabaseConstants;

/**
 * The persistent class for the userinfo database table.
 * 
 */
@Entity
@Table(name = DatabaseConstants.USERINFO_TABLE)
@NamedQueries({
        @NamedQuery(name = DatabaseConstants.FIND_ALL_USERINFO, query = "SELECT u FROM UserInfo u"),
        @NamedQuery(name = DatabaseConstants.FIND_USERINFO_FROM_USER, query = "SELECT u FROM UserInfo u WHERE u.user = :user"),
        @NamedQuery(name = DatabaseConstants.FIND_USERINFO_FROM_EMAILID, query = "SELECT u FROM UserInfo u WHERE u.mail = :mail") })
public class UserInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = DatabaseConstants.GUID)
    private int guid;

    @Column(name = DatabaseConstants.EXPERIENCE)
    private BigDecimal experience;

    @Column(name = DatabaseConstants.MAIL, length = DatabaseConstants.LENGTH_OF_MAIL)
    private String mail;

    @Column(name = DatabaseConstants.PHONE, length = DatabaseConstants.LENGTH_OF_PHONE)
    private String phone;

    // bi-directional many-to-one association to User
    @ManyToOne
    @JoinColumn(name = DatabaseConstants.USERID)
    private User user;

    public UserInfo() {
        Random random = new Random();
        guid = random.nextInt(1000000);
    }

    public int getGuid() {
        return this.guid;
    }

    public BigDecimal getExperience() {
        return this.experience;
    }

    public void setExperience(BigDecimal experience) {
        this.experience = experience;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}