package com.rakuten.online.test.persistence;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.rakuten.online.test.commons.util.DatabaseConstants;

/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@Table(name = DatabaseConstants.USER_TABLE)
@NamedQuery(name = DatabaseConstants.FIND_ALL_USERS, query = "SELECT u FROM User u")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = DatabaseConstants.USERID)
    private int id;

    @Column(name = DatabaseConstants.ISADMIN)
    private byte isAdmin;

    @Column(name = DatabaseConstants.NAME, length = DatabaseConstants.LENGTH_OF_NAME)
    private String name;

    @Column(name = DatabaseConstants.FIELD, length = DatabaseConstants.LENGTH_OF_PASSWORD)
    private String password;

    // bi-directional many-to-one association to SessionInfo
    @OneToMany(mappedBy = DatabaseConstants.MAPPED_USER)
    private transient List<SessionInfo> sessionInfos;

    // bi-directional many-to-one association to Userinfo
    @OneToMany(mappedBy = DatabaseConstants.MAPPED_USER)
    private transient List<UserInfo> userInfos;

    public User() {
        Random random = new Random();
        id = random.nextInt(10000);
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte getIsAdmin() {
        return this.isAdmin;
    }

    public void setIsAdmin(byte isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<SessionInfo> getSessionInfos() {
        return this.sessionInfos;
    }

    public void setSessionInfos(List<SessionInfo> sessionInfos) {
        this.sessionInfos = sessionInfos;
    }

    public SessionInfo addSessionInfo(SessionInfo sessionInfo) {
        getSessionInfos().add(sessionInfo);
        sessionInfo.setUser(this);

        return sessionInfo;
    }

    public SessionInfo removeSessionInfo(SessionInfo sessionInfo) {
        getSessionInfos().remove(sessionInfo);
        sessionInfo.setUser(null);

        return sessionInfo;
    }

    public List<UserInfo> getUserinfos() {
        return this.userInfos;
    }

    public void setUserinfos(List<UserInfo> userInfos) {
        this.userInfos = userInfos;
    }

    public UserInfo addUserinfo(UserInfo userInfo) {
        getUserinfos().add(userInfo);
        userInfo.setUser(this);

        return userInfo;
    }

    public UserInfo removeUserinfo(UserInfo userInfo) {
        getUserinfos().remove(userInfo);
        userInfo.setUser(null);

        return userInfo;
    }

}