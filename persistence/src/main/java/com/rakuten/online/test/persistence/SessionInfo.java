package com.rakuten.online.test.persistence;

import java.io.Serializable;

import javax.persistence.*;

import com.rakuten.online.test.commons.util.DatabaseConstants;

import java.math.BigDecimal;
import java.util.List;

/**
 * The persistent class for the session_info database table.
 * 
 */
@Entity
@Table(name = DatabaseConstants.SESSIONINFO_TABLE)
@NamedQuery(name = DatabaseConstants.FIND_ALL_SESSIONINFO, query = "SELECT s FROM SessionInfo s")
public class SessionInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = DatabaseConstants.SESSION_ID)
    private int sessionId;

    @Column(name = DatabaseConstants.INFO, length = DatabaseConstants.LENGTH_OF_INFO)
    private String info;

    @Column(name = DatabaseConstants.TIME_TAKEN)
    private BigDecimal timeTaken;

    // bi-directional many-to-one association to User
    @ManyToOne(cascade = { CascadeType.REMOVE })
    private User user;

    // bi-directional many-to-one association to UserTestHistory
    @OneToMany(mappedBy = DatabaseConstants.MAPPED_SESSIONINFO)
    private transient List<UserTestHistory> userTestHistories;

    public SessionInfo() {
    }

    public int getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public String getInfo() {
        return this.info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public BigDecimal getTimeTaken() {
        return this.timeTaken;
    }

    public void setTimeTaken(BigDecimal timeTaken) {
        this.timeTaken = timeTaken;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<UserTestHistory> getUsertesthistories() {
        return this.userTestHistories;
    }

    public void setUsertesthistories(List<UserTestHistory> userTestHistories) {
        this.userTestHistories = userTestHistories;
    }

    public UserTestHistory addUsertesthistory(UserTestHistory userTestHistory) {
        getUsertesthistories().add(userTestHistory);
        userTestHistory.setSessionInfo(this);

        return userTestHistory;
    }

    public UserTestHistory removeUsertesthistory(UserTestHistory userTestHistory) {
        getUsertesthistories().remove(userTestHistory);
        userTestHistory.setSessionInfo(null);

        return userTestHistory;
    }

}