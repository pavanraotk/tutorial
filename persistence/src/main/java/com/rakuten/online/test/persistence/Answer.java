package com.rakuten.online.test.persistence;

import java.io.Serializable;
import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.rakuten.online.test.commons.util.DatabaseConstants;

/**
 * The persistent class for the answer database table.
 * 
 */
@Entity
@Table(name = DatabaseConstants.ANSWER_TABLE)
@NamedQueries({
		@NamedQuery(name = DatabaseConstants.FIND_ALL_ANSWERS, query = "SELECT a FROM Answer a"),
		@NamedQuery(name = DatabaseConstants.FIND_ANSWER_BY_QUESTION, query = "SELECT a FROM Answer a WHERE a.question = :question") })
public class Answer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = DatabaseConstants.ANSWER_ID)
	private int aId;

	@Column(name = DatabaseConstants.ANSWER, length = DatabaseConstants.LENGTH_OF_ANSWER)
	private String ans;

	// bi-directional many-to-one association to Question
	@ManyToOne(targetEntity = Question.class)
	@JoinColumn(name = DatabaseConstants.QUESTION_ID)
	private Question question;

	public Answer() {
		Random random = new Random();
		this.aId = random.nextInt(1000);
	}

	public int getAId() {
		return this.aId;
	}

	public void setAId(int aId) {
		this.aId = aId;
	}

	public String getAns() {
		return this.ans;
	}

	public void setAns(String ans) {
		this.ans = ans;
	}

	public Question getQuestion() {
		return this.question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	@Override
	public String toString() {
		return "Answer [aId=" + aId + ", ans=" + ans + ", qId="
				+ question.getQId() + "]";
	}

}