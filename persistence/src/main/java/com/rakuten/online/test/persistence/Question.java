package com.rakuten.online.test.persistence;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.rakuten.online.test.commons.util.DatabaseConstants;

/**
 * The persistent class for the question database table.
 * 
 */
@Entity
@Table(name = DatabaseConstants.QUESTION_TABLE)
@NamedQuery(name = DatabaseConstants.FIND_ALL_QUESTIONS, query = "SELECT q FROM Question q")
public class Question implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = DatabaseConstants.QUESTION_ID)
	private int qId;

	@Column(name = DatabaseConstants.CATEGORY)
	private int category;

	@Column(name = DatabaseConstants.CORRECTANSWERIDS, length = DatabaseConstants.LENGTH_OF_CORRECTANSWERIDS)
	private String correctAnswerIds;

	@Column(name = DatabaseConstants.QUESTION, length = DatabaseConstants.LENGTH_OF_QUESTION, unique = true)
	private String questionInTable;

	@Column(name = DatabaseConstants.WEIGTHAGE)
	private int weightage;

	// bi-directional many-to-one association to Answer
	@OneToMany(mappedBy = DatabaseConstants.MAPPED_QUESTION, targetEntity = Answer.class, orphanRemoval = true)
	private transient List<Answer> answers;

	// bi-directional many-to-one association to UserTestHistory
	@OneToMany(mappedBy = DatabaseConstants.MAPPED_QUESTION)
	private transient List<UserTestHistory> userTestHistories;

	// bi-directional many-to-many association to Test
	@ManyToMany(mappedBy = DatabaseConstants.MAPPED_BY_QUESTIONS, cascade = { CascadeType.ALL })
	private transient List<Test> tests;

	public Question() {
		Random random = new Random();
		this.qId = random.nextInt(1000);
	}

	public int getQId() {
		return this.qId;
	}

	public void setQId(int qId) {
		this.qId = qId;
	}

	public int getCategory() {
		return this.category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getCorrectAnswerIds() {
		return this.correctAnswerIds;
	}

	public void setCorrectAnswerIds(String correctAnswerIds) {
		this.correctAnswerIds = correctAnswerIds;
	}

	public String getQuestion() {
		return this.questionInTable;
	}

	public void setQuestion(String question) {
		this.questionInTable = question;
	}

	public int getWeightage() {
		return this.weightage;
	}

	public void setWeightage(int weightage) {
		this.weightage = weightage;
	}

	public List<Answer> getAnswers() {
		return this.answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public Answer addAnswer(Answer answer) {
		getAnswers().add(answer);
		answer.setQuestion(this);

		return answer;
	}

	public Answer removeAnswer(Answer answer) {
		getAnswers().remove(answer);
		answer.setQuestion(null);

		return answer;
	}

	public List<UserTestHistory> getUsertesthistories() {
		return this.userTestHistories;
	}

	public void setUsertesthistories(List<UserTestHistory> userTestHistories) {
		this.userTestHistories = userTestHistories;
	}

	public UserTestHistory addUsertesthistory(UserTestHistory userTestHistory) {
		getUsertesthistories().add(userTestHistory);
		userTestHistory.setQuestion(this);

		return userTestHistory;
	}

	public UserTestHistory removeUsertesthistory(UserTestHistory userTestHistory) {
		getUsertesthistories().remove(userTestHistory);
		userTestHistory.setQuestion(null);

		return userTestHistory;
	}

	public List<Test> getTests() {
		return this.tests;
	}

	public void setTests(List<Test> tests) {
		this.tests = tests;
	}

	@Override
	public String toString() {
		return "Question [qId=" + qId + ", category=" + category
				+ ", correctAnswerIds=" + correctAnswerIds
				+ ", questionInTable=" + questionInTable + ", weightage="
				+ weightage + ", answers=" + answers + "]";
	}
}