package com.rakuten.online.test.persistence;

import java.io.Serializable;
import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.rakuten.online.test.commons.util.DatabaseConstants;

/**
 * The persistent class for the usertesthistory database table.
 * 
 */
@Entity
@Table(name = DatabaseConstants.USERTESTHISTORY_TABLE)
@NamedQuery(name = DatabaseConstants.FIND_ALL_USERTESTHISTORY, query = "SELECT u FROM UserTestHistory u")
public class UserTestHistory implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = DatabaseConstants.GUID)
    private int guid;

    @Column(name = DatabaseConstants.INPUT_ANSWER_IDS, length = DatabaseConstants.LENGTH_OF_INPUTANSWERIDS)
    private String inputAnswerIds;

    private byte result;

    // bi-directional many-to-one association to Question
    @ManyToOne
    @JoinColumn(name = DatabaseConstants.QUESTION_ID)
    private Question question;

    // bi-directional many-to-one association to SessionInfo
    @ManyToOne
    @JoinColumn(name = DatabaseConstants.SESSION_ID)
    private SessionInfo sessionInfo;

    public UserTestHistory() {
        Random random = new Random();
        guid = random.nextInt(1000000);
    }

    public int getGuid() {
        return this.guid;
    }

    public String getInputAnswerIds() {
        return this.inputAnswerIds;
    }

    public void setInputAnswerIds(String inputAnswerIds) {
        this.inputAnswerIds = inputAnswerIds;
    }

    public byte getResult() {
        return this.result;
    }

    public void setResult(byte result) {
        this.result = result;
    }

    public Question getQuestion() {
        return this.question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public SessionInfo getSessionInfo() {
        return this.sessionInfo;
    }

    public void setSessionInfo(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

}