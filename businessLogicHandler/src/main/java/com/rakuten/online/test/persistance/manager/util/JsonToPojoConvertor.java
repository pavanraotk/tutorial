package com.rakuten.online.test.persistance.manager.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rakuten.online.test.commons.util.Constants;
import com.rakuten.online.test.commons.util.JSONParser;
import com.rakuten.online.test.persistence.Answer;
import com.rakuten.online.test.persistence.Question;
import com.rakuten.online.test.persistence.Test;

public class JsonToPojoConvertor {

	private static final Logger LOG = LoggerFactory
			.getLogger(JsonToPojoConvertor.class);

	private JsonToPojoConvertor() {
	}

	public static Test getTestFromJson(String json) {
		Test test = new Test();
		try {
			JSONObject actualObject = new JSONObject(json);
			Map<String, Object> testMap = JSONParser.parseJson(actualObject);
			test.setCategory(Integer.parseInt((String) testMap
					.get(Constants.CATEGORY)));
			test.setDuration(Integer.parseInt((String) testMap
					.get(Constants.DURATION)));
			test.setExperience(new BigDecimal((String) testMap
					.get(Constants.EXPERIENCE)));
			if ((JSONArray) testMap.get(Constants.QUESTIONS) != null)
				test.setQuestions(getQuestionsFromJSONArray(
						(JSONArray) testMap.get(Constants.QUESTIONS), test));
			if (testMap.get(Constants.TESTID) != null) {
				test.setTestId(Integer.parseInt((String) testMap
						.get(Constants.TESTID)));
			}
		} catch (JSONException e) {
			LOG.error("Error in getting test from json {}", e);
		}

		return test;
	}

	private static List<Question> getQuestionsFromJSONArray(
			JSONArray jsonArray, Test test) throws JSONException {
		List<Question> questions = new ArrayList<>();
		List<Map<String, Object>> questionsList = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < jsonArray.length(); i++) {
			questionsList.add(i,
					JSONParser.parseJson(jsonArray.getJSONObject(i)));
		}

		for (Map<String, Object> question : questionsList) {
			Question tempQuestion = new Question();
			tempQuestion.setCategory(Integer.parseInt((String) question
					.get(Constants.CATEGORY)));
			tempQuestion.setCorrectAnswerIds((String) question
					.get(Constants.CORRECTANSWERIDS));
			tempQuestion.setQuestion((String) question.get(Constants.QUESTION));
			tempQuestion.setWeightage(Integer.parseInt((String) question
					.get(Constants.WEIGHTAGE)));
			tempQuestion
					.setAnswers(getAnswersFromJSONObjectForQuestion(
							(JSONObject) question.get(Constants.ANSWERS),
							tempQuestion));
			if (question.get(Constants.QID) != null) {
				tempQuestion.setQId(Integer.parseInt((String) question.get(Constants.QID)));
			}
			List<Test> testList = tempQuestion.getTests();
			if (testList != null)
				testList.add(test);
			else {
				testList = new ArrayList<Test>();
				testList.add(test);
			}
			tempQuestion.setTests(testList);
			questions.add(tempQuestion);
		}
		return questions;
	}

	public static Question getQuestionFromJson(String questionJsonObjectString)
			throws JSONException {

		Question question = new Question();
		Map<String, Object> questionJsonObject = JSONParser
				.parseJson(new JSONObject(questionJsonObjectString));
		question.setCategory(Integer.parseInt((String) questionJsonObject
				.get(Constants.CATEGORY)));
		question.setCorrectAnswerIds((String) questionJsonObject
				.get(Constants.CORRECTANSWERIDS));
		question.setQuestion((String) questionJsonObject
				.get(Constants.QUESTION));
		question.setWeightage(Integer.parseInt((String) questionJsonObject
				.get(Constants.WEIGHTAGE)));
		if (questionJsonObject.get(Constants.QID) != null) {
			question.setQId(Integer.parseInt((String) questionJsonObject.get(Constants.QID)));
		}
		question.setAnswers(getAnswersFromJSONObjectForQuestion(
				(JSONObject) questionJsonObject.get(Constants.ANSWERS),
				question));
		return question;
	}

	private static List<Answer> getAnswersFromJSONObjectForQuestion(
			JSONObject jsonObject, Question question) throws JSONException {
		List<Answer> answers = new ArrayList<Answer>();
		String a = jsonObject.getString("a");
		String b = jsonObject.getString("b");
		String c = jsonObject.getString("c");
		String d = jsonObject.getString("d");

		Answer answer1 = new Answer();
		answer1.setAns(a);
		answer1.setQuestion(question);

		Answer answer2 = new Answer();
		answer2.setAns(b);
		answer2.setQuestion(question);

		Answer answer3 = new Answer();
		answer3.setAns(c);
		answer3.setQuestion(question);

		Answer answer4 = new Answer();
		answer4.setAns(d);
		answer4.setQuestion(question);

		answers.add(answer1);
		answers.add(answer2);
		answers.add(answer3);
		answers.add(answer4);

		return answers;
	}

}
