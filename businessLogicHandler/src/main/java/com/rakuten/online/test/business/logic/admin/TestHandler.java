package com.rakuten.online.test.business.logic.admin;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rakuten.online.test.commons.exception.HandlerException;
import com.rakuten.online.test.commons.util.Constants;
import com.rakuten.online.test.persistance.manager.util.JsonToPojoConvertor;
import com.rakuten.online.test.persistence.Test;
import com.rakuten.online.test.persistence.UserInfo;
import com.rakuten.online.test.persistence.manager.TestDAOImpl;
import com.rakuten.online.test.persistence.manager.UserInfoDAOImpl;

public class TestHandler {
    private static final Logger LOG = LoggerFactory
            .getLogger(TestHandler.class);

    TestDAOImpl testDAO;
    UserInfoDAOImpl userInfoDAO;

    /**
     * This method is used to add the test to the DB.
     * 
     * @param testJson
     *            object, where the entire test will be sent as JSON object
     * 
     * @return
     * @throws HandlerException
     */
    public JSONObject addTest(String testJsonObject) throws HandlerException {
        Test testObject = null;
        JSONObject returnObject = new JSONObject();
        try {
            testObject = JsonToPojoConvertor.getTestFromJson(testJsonObject);
            this.getTestDAO().addTest(testObject);
            returnObject.put(Constants.RESULT,
                    Constants.SUCCESSFULLY_ADDED_TEST);
        } catch (Exception ex) {
            LOG.error("Error in adding test. {}", ex);
            throw new HandlerException("Error in adding test " + ex);
        }
        return returnObject;
    }

    /**
     * Removes the test from the given testId.
     * 
     * @param testId
     * @return
     */
    public JSONObject removeTest(String testId) throws HandlerException {
        JSONObject returnObject = new JSONObject();
        try {
            this.getTestDAO().deleteTest(
                    getTestDAO().getTest(Integer.parseInt(testId)));
            returnObject.put(Constants.RESULT,
                    Constants.SUCCESSFULLY_REMOVED_TEST);
        } catch (Exception ex) {
            LOG.error("Error in removing test {}", ex);
            throw new HandlerException("Error in removing test " + ex);
        }
        return returnObject;
    }

    /**
     * This is the API to update the test.
     * 
     * @param updatedTest
     * @return
     */
    public JSONObject updateTest(String updatedTestJson)
            throws HandlerException {
        JSONObject returnObject = new JSONObject();
        try {
            Test updatedtestObject = JsonToPojoConvertor
                    .getTestFromJson(updatedTestJson);
            this.getTestDAO().updateTest(updatedtestObject);
            returnObject.put(Constants.RESULT,
                    Constants.SUCCESSFULLY_UPDATED_TEST);
        } catch (Exception ex) {
            LOG.error("Error in updated test {}", ex);
            throw new HandlerException("Error in updated test " + ex);
        }
        return returnObject;
    }

    /**
     * 
     * @param testId
     * @return JSONObject containing the test object
     * @throws HandlerException
     */
    public JSONObject getTestByTestId(String testId) throws HandlerException {
        JSONObject returnObject = new JSONObject();
        try {
            Test test = this.getTestDAO().getTest(Integer.parseInt(testId));
            returnObject.put(Constants.TEST, test);
            returnObject.put(Constants.RESULT,
                    Constants.SUCCESSFULLY_GOT_TEST_BY_ID);
        } catch (Exception ex) {
            LOG.error("Error in getting test by Id {} with exception {}",
                    testId, ex);
            throw new HandlerException("Error in getting test by Id " + testId
                    + Constants.WITH_EXCEPTION + ex);
        }
        return returnObject;
    }

    /**
     * 
     * @param category
     * @return Tests by given category
     */
    public JSONObject getTestsByCategory(String category)
            throws HandlerException {
        JSONObject returnObject = new JSONObject();
        try {
            List<Test> testList = this.getTestDAO().getTestByCategory(
                    Integer.parseInt(category));
            returnObject.put(Constants.TEST_LIST, testList);
            returnObject.put(Constants.RESULT,
                    Constants.SUCCESSFULLY_GOT_TEST_BY_CATEGORY);
        } catch (Exception ex) {
            LOG.error(
                    "Error in getting tests by category {} with exception {}",
                    category, ex);
            throw new HandlerException("Error in getting tests by category "
                    + category + Constants.WITH_EXCEPTION + ex);
        }
        return returnObject;
    }

    /**
     * To get tests based on the experience.
     * 
     * @param experience
     * @return
     */
    public JSONObject getTestsByExperience(String experience)
            throws HandlerException {
        JSONObject returnObject = new JSONObject();
        try {
            List<Test> testList = this.getTestDAO().getTestByExperience(
                    new BigDecimal(experience));
            returnObject.put(Constants.TEST_LIST, testList);
            returnObject.put(Constants.RESULT,
                    Constants.SUCCESSFULLY_GOT_TEST_BY_EXPERIENCE);
        } catch (Exception ex) {
            LOG.error(
                    "Error in removing test for experience {} with exception{}",
                    experience, ex);
            throw new HandlerException("Error in getting tests by experience "
                    + experience + Constants.WITH_EXCEPTION + ex);
        }
        return returnObject;
    }

    public JSONObject scheduleTest(String userEmailId) throws HandlerException {
        JSONObject returnObject = new JSONObject();

        List<Test> testList = null;
        UserInfo userInfo = null;
        try {
            userInfo = userInfoDAO.getUserFromEmail(userEmailId);
            testList = testDAO.getTestByExperience(userInfo.getExperience());
            int listLength = testList.size();
            Random randomNumber = new Random();
            int nthTest = randomNumber.nextInt(listLength);
            returnObject.put(Constants.SCHEDULED_TEST, testList.get(nthTest));
            returnObject.put(Constants.RESULT,
                    Constants.SUCCESSFULLY_SCHEDULED_TEST);
        } catch (Exception ex) {
            LOG.error("Error in scheduling test with user {} for exception {}",
                    userEmailId, ex);
            throw new HandlerException("Error in scheduling test for "
                    + userEmailId + Constants.WITH_EXCEPTION + ex);
        }
        return returnObject;
    }

    public UserInfoDAOImpl getUserInfoDAO() {
        return userInfoDAO;
    }

    public void setUserInfoDAO(UserInfoDAOImpl userInfoDAO) {
        this.userInfoDAO = userInfoDAO;
    }

    public TestDAOImpl getTestDAO() {
        return testDAO;
    }

    public void setTestDAO(TestDAOImpl testDAO) {
        this.testDAO = testDAO;
    }

}
