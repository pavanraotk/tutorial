package com.rakuten.online.test.business.logic.admin;

import java.util.List;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rakuten.online.test.commons.exception.HandlerException;
import com.rakuten.online.test.commons.util.Constants;
import com.rakuten.online.test.persistance.manager.util.JsonToPojoConvertor;
import com.rakuten.online.test.persistence.Answer;
import com.rakuten.online.test.persistence.Question;
import com.rakuten.online.test.persistence.manager.AnswerDAOImpl;
import com.rakuten.online.test.persistence.manager.QuestionDAOImpl;

public class QuestionHandler {

	private static final Logger LOG = LoggerFactory
			.getLogger(QuestionHandler.class);

	QuestionDAOImpl questionDAO;
	AnswerDAOImpl answerDAO;

	public JSONObject addQuestion(String questionJsonObject)
			throws HandlerException {
		Question questionObject;
		List<Answer> answers = null;
		JSONObject returnObject = new JSONObject();
		try {
			questionObject = JsonToPojoConvertor
					.getQuestionFromJson(questionJsonObject);
			answers = questionObject.getAnswers();
			this.getQuestionDAO().addQuestion(questionObject);
			this.getAnswerDAO().addAnswer(answers);
			returnObject.put(Constants.RESULT,
					Constants.SUCCESSFULLY_ADDED_QUESTION);
		} catch (Exception ex) {
			LOG.error("Error in adding Question. {}", ex);
			throw new HandlerException("Error in adding Question " + ex);
		}
		return returnObject;
	}

	public JSONObject removeQuestion(String questionId) throws HandlerException {
		JSONObject returnObject = new JSONObject();
		try {
			Question question = this.getQuestionDAO().getQuestion(
					Integer.parseInt(questionId));
			List<Answer> answers = this.answerDAO.getAnswersByQuestion(question);
			for (Answer answer : answers)
				this.getAnswerDAO().deleteAnswer(answer);
			this.getQuestionDAO().deleteQuestion(question);
			returnObject.put(Constants.RESULT,
					Constants.SUCCESSFULLY_REMOVED_QUESTION);
		} catch (Exception ex) {
			LOG.error("Error in removing Question {}", ex);
			throw new HandlerException("Error in removing Question " + ex);
		}
		return returnObject;
	}

	public JSONObject updateQuestion(String updatedQuestionJsonObject)
			throws HandlerException {
		JSONObject returnObject = new JSONObject();
		try {
			Question updatedquestionObject = JsonToPojoConvertor
					.getQuestionFromJson(updatedQuestionJsonObject);
			this.getQuestionDAO().updateQuestion(updatedquestionObject);
			returnObject.put(Constants.RESULT,
					Constants.SUCCESSFULLY_UPDATED_QUESTION);
		} catch (Exception ex) {
			LOG.error("Error in updated test {}", ex);
			throw new HandlerException("Error in updated test " + ex);
		}
		return returnObject;
	}

	public JSONObject getQuestionById(String questionId)
			throws HandlerException {
		JSONObject returnObject = new JSONObject();
		try {
			Question question = this.getQuestionDAO().getQuestion(
					Integer.parseInt(questionId));
			returnObject.put(Constants.QUESTION, question);
			returnObject.put(Constants.RESULT,
					Constants.SUCCESSFULLY_GOT_QUESTION_BY_ID);
		} catch (Exception ex) {
			LOG.error("Error in getting question by Id {} with exception {}",
					questionId, ex);
			throw new HandlerException("Error in getting question by Id "
					+ questionId + Constants.WITH_EXCEPTION + ex);
		}
		return returnObject;
	}

	public QuestionDAOImpl getQuestionDAO() {
		return questionDAO;
	}

	public void setQuestionDAO(QuestionDAOImpl questionDAO) {
		this.questionDAO = questionDAO;
	}

	public AnswerDAOImpl getAnswerDAO() {
		return answerDAO;
	}

	public void setAnswerDAO(AnswerDAOImpl answerDAO) {
		this.answerDAO = answerDAO;
	}

}
