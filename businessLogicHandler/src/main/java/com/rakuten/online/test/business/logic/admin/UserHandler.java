package com.rakuten.online.test.business.logic.admin;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rakuten.online.test.commons.exception.HandlerException;
import com.rakuten.online.test.commons.util.Constants;
import com.rakuten.online.test.commons.util.JSONParser;
import com.rakuten.online.test.persistence.User;
import com.rakuten.online.test.persistence.UserInfo;
import com.rakuten.online.test.persistence.manager.UserDAOImpl;
import com.rakuten.online.test.persistence.manager.UserInfoDAOImpl;

public class UserHandler {

    private static final Logger LOG = LoggerFactory
            .getLogger(UserHandler.class);

    UserDAOImpl userDAO;
    UserInfoDAOImpl userInfoDAO;

    /**
     * @param json
     *            Call the json parser to obtain map. From the map, generate the
     *            user and userInfo object Call the DAO to persist the user and
     *            userInfo.
     */
    public JSONObject addUser(String jsonString) throws HandlerException {
        User user = null;
        UserInfo userInfo = new UserInfo();
        JSONObject returnObject = new JSONObject();
        try {
            JSONObject json = this.getJSONFromString(jsonString);
            Map<String, Object> map = JSONParser.parseJson(json);
            user = getUserFromMap(map);
            userInfo = getUserInfoFromMap(map);
            userInfo.setUser(user);
            this.getUserDAO().addUser(user);
            this.getUserInfoDAO().addUserInfo(userInfo);
            returnObject.put(Constants.RESULT,
                    Constants.SUCCESSFULLY_ADDED_USER);
        } catch (Exception ex) {
            LOG.error("Error in getting user {}", ex);
            throw new HandlerException("Error in getting user " + ex);
        }
        return returnObject;
    }

    /**
     * 
     * @param json
     * @return remove user and return confirmation
     */
    public JSONObject removeUser(String emailId) throws HandlerException {
        User user = null;
        UserInfo userInfo = null;
        JSONObject returnObject = new JSONObject();
        try {
            userInfo = this.getUserInfoDAO().getUserFromEmail(emailId);
            user = userInfo.getUser();
            this.getUserInfoDAO().deleteUser(userInfo);
            this.getUserDAO().deleteUser(user);
            returnObject.put(Constants.RESULT,
                    Constants.SUCCESSFULLY_REMOVED_USER);
        } catch (Exception ex) {
            LOG.error("Error in removing user {}", ex);
            throw new HandlerException("Error in removing user " + ex);
        }
        return returnObject;
    }

    /**
     * 
     * @param json
     * @return jsonObject response body after updating user
     */
    public JSONObject updateUser(String jsonString) throws HandlerException {
        JSONObject returnObject = new JSONObject();
        try {
            JSONObject json = this.getJSONFromString(jsonString);
            Map<String, Object> map = JSONParser.parseJson(json);
            User user = getUserFromMap(map);
            UserInfo userInfo = getUserInfoFromMap(map);
            this.getUserDAO().updateUser(user);
            this.getUserInfoDAO().updateUserInfo(userInfo);
            returnObject.put(Constants.RESULT,
                    Constants.SUCCESSFULLY_UPDATED_USER);
        } catch (Exception ex) {
            LOG.error("Error in updating user with exception {}", ex);
            throw new HandlerException("Error in updating user " + ex);
        }
        return returnObject;
    }

    /**
     * 
     * @param mailId
     * @return UserObject belonging to the given mail Id
     */
    public JSONObject getSingleUser(String emailId) throws HandlerException {
        JSONObject returnObject = new JSONObject();
        UserInfo userInfo = null;
        try {
            userInfo = this.getUserInfoDAO().getUserFromEmail(emailId);
            if (null != userInfo) {
                returnObject.put(Constants.NAME, userInfo.getUser().getName());
                returnObject.put(Constants.MAIL, userInfo.getMail());
                returnObject.put(Constants.PHONE, userInfo.getPhone());
                returnObject
                        .put(Constants.EXPERIENCE, userInfo.getExperience());
            } else {
                returnObject.put(Constants.RESULT, "User with mail Id "
                        + emailId + " is not found");
            }
        } catch (Exception ex) {
            LOG.error(
                    "Error in getting single user for given mail Id {} with exception {}",
                    emailId, ex);
            throw new HandlerException(
                    "Error in getting single user for given mail Id " + ex
                            + " with exception " + ex);
        }
        return returnObject;
    }

    /**
     * Get all users from user table
     * 
     * @return
     */
    public JSONObject getAllUsers() throws HandlerException {
        JSONObject returnObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        List<User> users;
        UserInfo userInfo;
        LOG.debug("Getting all users");
        try {
            users = this.getUserDAO().getAllUsers();
            for (User user : users) {
                JSONObject temporaryObject = new JSONObject();
                temporaryObject.put(Constants.NAME, user.getName());
                userInfo = this.getUserInfoDAO().getUser(user);
                temporaryObject.put(Constants.MAIL, userInfo.getMail());
                temporaryObject.put(Constants.PHONE, userInfo.getPhone());
                jsonArray.put(temporaryObject);
            }
            returnObject.put(Constants.RESULT, jsonArray);
        } catch (Exception ex) {
            LOG.error("Error in getting all users {}", ex);
            throw new HandlerException("Error in getting all users " + ex);
        }
        return returnObject;
    }

    /**
     * @param jsonString
     * @return JSONObject for json string
     */
    private JSONObject getJSONFromString(String jsonString) {
        LOG.debug("Getting json from string {}", jsonString);
        try {
            return new JSONObject(jsonString);
        } catch (JSONException ex) {
            LOG.error("Error in getting json from string {}", ex);
            return null;
        }
    }

    /**
     * 
     * @param map
     * @return Get user from the map obtained from json parser
     */
    private User getUserFromMap(Map<String, Object> map) {
        User user = new User();
        for (Map.Entry<String, Object> mapEntry : map.entrySet()) {
            if (mapEntry.getKey().equals(Constants.ISADMIN)) {
                int adminValue = Integer.parseInt((String) mapEntry.getValue());
                byte admin = (byte) adminValue;
                user.setIsAdmin(admin);
            } else if (mapEntry.getKey().equals(Constants.NAME)) {
                user.setName((String) mapEntry.getValue());
            } else if (mapEntry.getKey().equals(Constants.FIELD)) {
                user.setPassword((String) mapEntry.getValue());
            }
        }
        return user;
    }

    /**
     * 
     * @param map
     * @return Get userINfo object from the map obtained from json parser
     */
    private UserInfo getUserInfoFromMap(Map<String, Object> map) {
        UserInfo userInfo = new UserInfo();
        for (Map.Entry<String, Object> mapEntry : map.entrySet()) {
            if (mapEntry.getKey().equals(Constants.EXPERIENCE)) {
                userInfo.setExperience(BigDecimal.valueOf(Double
                        .valueOf((String) mapEntry.getValue())));
            } else if (mapEntry.getKey().equals(Constants.PHONE)) {
                userInfo.setPhone((String) mapEntry.getValue());
            } else if (mapEntry.getKey().equals(Constants.MAIL)) {
                userInfo.setMail((String) mapEntry.getValue());
            }
        }
        return userInfo;
    }

    public UserDAOImpl getUserDAO() {
        return userDAO;
    }

    public void setUserDAO(UserDAOImpl userDAO) {
        this.userDAO = userDAO;
    }

    public UserInfoDAOImpl getUserInfoDAO() {
        return userInfoDAO;
    }

    public void setUserInfoDAO(UserInfoDAOImpl userInfoDAO) {
        this.userInfoDAO = userInfoDAO;
    }

}
