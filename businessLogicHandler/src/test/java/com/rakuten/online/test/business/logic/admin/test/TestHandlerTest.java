package com.rakuten.online.test.business.logic.admin.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rakuten.online.test.business.logic.admin.TestHandler;
import com.rakuten.online.test.commons.exception.HandlerException;
import com.rakuten.online.test.commons.util.Constants;
import com.rakuten.online.test.commons.util.DatabaseException;
import com.rakuten.online.test.persistance.manager.util.JsonToPojoConvertor;
import com.rakuten.online.test.persistence.Test;
import com.rakuten.online.test.persistence.User;
import com.rakuten.online.test.persistence.UserInfo;
import com.rakuten.online.test.persistence.manager.TestDAOImpl;
import com.rakuten.online.test.persistence.manager.UserInfoDAOImpl;

public class TestHandlerTest {
	Logger LOG = LoggerFactory.getLogger(TestHandlerTest.class);

	TestHandler testHandler;
	TestDAOImpl testDAO;
	UserInfoDAOImpl userInfoDAO;
	List<Test> testList, dummyTestList;
	Test test = new Test();
	String testJsonString = "{\r\n    \"testId\": \"35932\",\r\n    \"category\": \"1\",\r\n    \"duration\": \"10\",\r\n    \"experience\": \"1\",\r\n    \"questions\": [\r\n        {\r\n            \"category\": \"1\",\r\n            \"correctAnswerIds\": \"Yes\",\r\n            \"question\": \"Is Java Object Oriented ?\",\r\n            \"weightage\": \"10\",\r\n            \"answers\": {\r\n                \"a\": \"Yes\",\r\n                \"b\": \"No\",\r\n                \"c\": \"Don't Know\",\r\n                \"d\": \"May Be\"\r\n            }\r\n        },\r\n        {\r\n            \"category\": \"1\",\r\n            \"correctAnswerIds\": \"Yes\",\r\n            \"question\": \"Is C Object Oriented ?\",\r\n            \"weightage\": \"10\",\r\n            \"answers\": {\r\n                \"a\": \"Yes\",\r\n                \"b\": \"No\",\r\n                \"c\": \"Don't Know\",\r\n                \"d\": \"May Be\"\r\n            }\r\n        }\r\n    ]\r\n}";
	String updatedTestJson = "{\r\n    \"testId\": \"35932\",\r\n    \"category\": \"2\",\r\n    \"duration\": \"20\",\r\n    \"experience\": \"1\",\r\n    \"questions\": [\r\n        {\r\n            \"category\": \"1\",\r\n            \"correctAnswerIds\": \"Yes\",\r\n            \"question\": \"Is Java Object Oriented ?\",\r\n            \"weightage\": \"10\",\r\n            \"answers\": {\r\n                \"a\": \"Yes\",\r\n                \"b\": \"No\",\r\n                \"c\": \"Don't Know\",\r\n                \"d\": \"May Be\"\r\n            }\r\n        },\r\n        {\r\n            \"category\": \"1\",\r\n            \"correctAnswerIds\": \"Yes\",\r\n            \"question\": \"Is C Object Oriented ?\",\r\n            \"weightage\": \"10\",\r\n            \"answers\": {\r\n                \"a\": \"Yes\",\r\n                \"b\": \"No\",\r\n                \"c\": \"I Know\",\r\n                \"d\": \"May Not Be\"\r\n            }\r\n        }\r\n    ]\r\n}";
	String testId = "35932";
	String userEmailId = "test@rakuten.com";

	int id;
	byte isAdmin;
	String name, password, mail, phone;
	BigDecimal experience;

	JSONObject result;

	@Before
	public void setUp() {

		result = new JSONObject();

		testHandler = new TestHandler();
		testDAO = EasyMock.createMock(TestDAOImpl.class);
		userInfoDAO = EasyMock.createMock(UserInfoDAOImpl.class);

		testHandler.setUserInfoDAO(userInfoDAO);
		testHandler.setTestDAO(testDAO);

		test = JsonToPojoConvertor.getTestFromJson(testJsonString);
		testList = new ArrayList<Test>();
		testList.add(test);

		dummyTestList = EasyMock.<List<Test>> anyObject();

		id = 12345;
		isAdmin = 0;
		name = "Junit";
		password = "JunitPassword";
		mail = "junit@rakuten.com";
		phone = "+912798967899";
		experience = new BigDecimal(1.0);

		User user = new User();
		user.setId(id);
		user.setIsAdmin(isAdmin);
		user.setName(name);
		user.setPassword(password);

		UserInfo userInfo = new UserInfo();
		userInfo.setExperience(experience);
		userInfo.setMail(mail);
		userInfo.setPhone(phone);
		userInfo.setUser(user);

		try {
			testDAO.addTest(test);
			EasyMock.expectLastCall().anyTimes();
			testDAO.deleteTest(test);
			EasyMock.expectLastCall().anyTimes();
			testDAO.updateTest(EasyMock.anyObject(Test.class));
			EasyMock.expectLastCall().anyTimes();
			EasyMock.expect(testDAO.getTest(test.getTestId())).andReturn(test)
					.anyTimes();
			EasyMock.expect(testDAO.getTestByCategory((test.getCategory())))
					.andReturn(testList).anyTimes();
			EasyMock.expect(testDAO.getTestByExperience(test.getExperience()))
					.andReturn(testList).anyTimes();
			EasyMock.replay(testDAO);
			EasyMock.expect(userInfoDAO.getUserFromEmail(mail))
					.andReturn(userInfo).anyTimes();
			EasyMock.replay(userInfoDAO);

		} catch (DatabaseException e) {
			LOG.error("Error in testDAO {}", e);
		}

	}

	@After
	public void tearDown() {
		testHandler = null;
	}

	@org.junit.Test
	public void testAddTest() {
		try {
			result = testHandler.addTest(testJsonString);
			Assert.assertEquals(Constants.SUCCESSFULLY_ADDED_TEST,
					result.get(Constants.RESULT));
		} catch (JSONException ex) {
			Assert.fail(Constants.WRONG_JSON_RETURNED + ex);
		} catch (HandlerException ex) {
			Assert.fail(" Error in adding test " + ex);
		}
	}

	@org.junit.Test
	public void testRemoveTest() {
		try {
			result = testHandler.removeTest(String.valueOf(test.getTestId()));
			Assert.assertEquals(Constants.SUCCESSFULLY_REMOVED_TEST,
					result.get(Constants.RESULT));
		} catch (JSONException ex) {
			Assert.fail(Constants.WRONG_JSON_RETURNED + ex);
		} catch (HandlerException ex) {
			Assert.fail(" Error in removing test " + ex);
		}
	}

	@org.junit.Test
	public void testUpdateTest() {
		try {
			result = testHandler.updateTest(updatedTestJson);
			Assert.assertEquals(Constants.SUCCESSFULLY_UPDATED_TEST,
					result.get(Constants.RESULT));
		} catch (JSONException ex) {
			Assert.fail(Constants.WRONG_JSON_RETURNED + ex);
		} catch (HandlerException ex) {
			Assert.fail(" Error in updating test " + ex);
		}
	}

	@org.junit.Test
	public void testGetTestById() {
		try {
			result = testHandler.getTestByTestId(String.valueOf(test
					.getTestId()));
			Assert.assertEquals(Constants.SUCCESSFULLY_GOT_TEST_BY_ID,
					result.get(Constants.RESULT));
		} catch (JSONException ex) {
			Assert.fail(Constants.WRONG_JSON_RETURNED + ex);
		} catch (HandlerException ex) {
			Assert.fail(Constants.ERROR_IN_GETTING_TEST + ex);
		}
	}

	@org.junit.Test
	public void testGetTestByCategory() {
		try {
			result = testHandler.getTestsByCategory(String.valueOf(test
					.getCategory()));
			Assert.assertEquals(Constants.SUCCESSFULLY_GOT_TEST_BY_CATEGORY,
					result.get(Constants.RESULT));
		} catch (JSONException ex) {
			Assert.fail(Constants.WRONG_JSON_RETURNED + ex);
		} catch (HandlerException ex) {
			Assert.fail(Constants.ERROR_IN_GETTING_TEST + ex);
		}
	}

	@org.junit.Test
	public void testGetTestByExperience() {
		try {
			result = testHandler.getTestsByExperience(String.valueOf(test
					.getExperience()));
			Assert.assertEquals(Constants.SUCCESSFULLY_GOT_TEST_BY_EXPERIENCE,
					result.get(Constants.RESULT));
		} catch (JSONException ex) {
			Assert.fail(Constants.WRONG_JSON_RETURNED + ex);
		} catch (HandlerException ex) {
			Assert.fail(Constants.ERROR_IN_GETTING_TEST + ex);
		}
	}

	@org.junit.Test
	public void testScheduelTest() {
		try {
			result = testHandler.scheduleTest(mail);
			Assert.assertEquals(Constants.SUCCESSFULLY_SCHEDULED_TEST,
					result.get(Constants.RESULT));
		} catch (JSONException ex) {
			Assert.fail(Constants.WRONG_JSON_RETURNED + ex);
		} catch (HandlerException ex) {
			Assert.fail("Error in scheduling test" + ex);
		}
	}
}
