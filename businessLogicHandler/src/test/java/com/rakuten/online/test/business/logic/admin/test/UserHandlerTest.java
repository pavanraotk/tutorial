package com.rakuten.online.test.business.logic.admin.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rakuten.online.test.business.logic.admin.UserHandler;
import com.rakuten.online.test.commons.exception.HandlerException;
import com.rakuten.online.test.commons.util.Constants;
import com.rakuten.online.test.commons.util.DatabaseException;
import com.rakuten.online.test.persistence.User;
import com.rakuten.online.test.persistence.UserInfo;
import com.rakuten.online.test.persistence.manager.UserDAOImpl;
import com.rakuten.online.test.persistence.manager.UserInfoDAOImpl;

public class UserHandlerTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(UserHandlerTest.class);

	UserHandler userHandler;

	String addUserJson, deleteUserJson;

	JSONObject successfulJson;

	int id;
	byte isAdmin;
	String name, password, mail, phone;
	BigDecimal experience;

	UserDAOImpl userDAO;
	UserInfoDAOImpl userInfoDAO;

	UserInfo userInfo = new UserInfo();
	User user = new User();
	List<User> users;

	@Before
	public void setUp() {
		userHandler = new UserHandler();

		userDAO = EasyMock.createMock(UserDAOImpl.class);
		userInfoDAO = EasyMock.createMock(UserInfoDAOImpl.class);

		userHandler.setUserDAO(userDAO);
		userHandler.setUserInfoDAO(userInfoDAO);

		users = new ArrayList<User>();

		id = 12345;
		isAdmin = 0;
		name = "Junit";
		password = "JunitPassword";
		mail = "junit@rakuten.com";
		phone = "+912798967899";
		experience = new BigDecimal(1.0);

		addUserJson = "{" + "\"name\": \"Pavan\"," + "\"isAdmin\": \"1\","
				+ "\"password\": \"Anfield777\"," + "\"experience\": \"4.0\","
				+ "\"mail\": \"pavan.rao333@gmail.com\","
				+ "\"phone\": \"+919663370912\"" + "}";
		deleteUserJson = mail;

		user.setIsAdmin(isAdmin);
		user.setName(name);
		user.setPassword(password);

		userInfo.setExperience(experience);
		userInfo.setMail(mail);
		userInfo.setPhone(phone);
		userInfo.setUser(user);

		users.add(user);

		try {
			userDAO.addUser(EasyMock.anyObject(User.class));
			EasyMock.expectLastCall().anyTimes();
			userDAO.deleteUser(EasyMock.anyObject(User.class));
			EasyMock.expectLastCall().anyTimes();
			userDAO.updateUser(EasyMock.anyObject(User.class));
			EasyMock.expectLastCall().anyTimes();
			EasyMock.expect(userDAO.getAllUsers()).andReturn(users).anyTimes();
			EasyMock.replay(userDAO);

			userInfoDAO.addUserInfo(EasyMock.anyObject(UserInfo.class));
			EasyMock.expectLastCall().anyTimes();
			EasyMock.expect(userInfoDAO.getUserFromEmail(mail))
					.andReturn(userInfo).anyTimes();
			userInfoDAO.updateUserInfo(EasyMock.anyObject(UserInfo.class));
			EasyMock.expectLastCall().anyTimes();
			EasyMock.expect(userInfoDAO.getUser(user)).andReturn(userInfo)
					.anyTimes();
			userInfoDAO.deleteUser(userInfo);
			EasyMock.expectLastCall().anyTimes();
			EasyMock.replay(userInfoDAO);
		} catch (DatabaseException e) {
			LOG.error("Error in mocking of add user");
		}

	}

	@Test
	public void tearDown() {
		userHandler = null;
	}

	@Test
	public void testAddUser() {
		try {
			successfulJson = userHandler.addUser(addUserJson);
			Assert.assertEquals(Constants.SUCCESSFULLY_ADDED_USER,
					successfulJson.get(Constants.RESULT));
		} catch (JSONException ex) {
			Assert.fail("Wrong json returned " + ex);
		} catch (HandlerException ex) {
			Assert.fail("Exception occurred " + ex);
		}
	}

	@Test
	public void testDeleteUser() {
		try {
			successfulJson = userHandler.removeUser(deleteUserJson);
			Assert.assertEquals(Constants.SUCCESSFULLY_REMOVED_USER,
					successfulJson.get(Constants.RESULT));
		} catch (JSONException ex) {
			Assert.fail("Wrong json returned " + ex);
		} catch (HandlerException ex) {
			Assert.fail("Exception occurred " + ex);
		}
	}

	@Test
	public void testUpdateUser() {
		try {
			successfulJson = userHandler.updateUser(addUserJson);
			Assert.assertEquals(Constants.SUCCESSFULLY_UPDATED_USER,
					successfulJson.get(Constants.RESULT));
		} catch (JSONException ex) {
			Assert.fail("Wrong json returned " + ex);
		} catch (HandlerException ex) {
			Assert.fail("Exception occurred " + ex);
		}
	}

	@Test
	public void testGetSingleUser() {
		try {
			successfulJson = userHandler.getSingleUser(mail);
			Assert.assertEquals(name, successfulJson.get(Constants.NAME));
		} catch (JSONException ex) {
			Assert.fail("Wrong json returned " + ex);
		} catch (HandlerException ex) {
			Assert.fail("Exception occurred " + ex);
		}
	}

	@Test
	public void testGetAllUser() {
		try {
			successfulJson = userHandler.getAllUsers();
		} catch (HandlerException ex) {
			Assert.fail("Exception occurred " + ex);
		}
	}

}
