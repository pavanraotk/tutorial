package com.rakuten.online.test.business.logic.admin.test;

import java.util.List;

import junit.framework.Assert;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rakuten.online.test.business.logic.admin.QuestionHandler;
import com.rakuten.online.test.commons.exception.HandlerException;
import com.rakuten.online.test.commons.util.Constants;
import com.rakuten.online.test.commons.util.DatabaseException;
import com.rakuten.online.test.persistence.Answer;
import com.rakuten.online.test.persistence.Question;
import com.rakuten.online.test.persistence.manager.AnswerDAOImpl;
import com.rakuten.online.test.persistence.manager.QuestionDAOImpl;

public class QuestionHandlerTest {

	Logger LOG = LoggerFactory.getLogger(QuestionHandlerTest.class);
	QuestionDAOImpl questionDAO;
	AnswerDAOImpl answerDAO;
	QuestionHandler questionHandler;
	String questionJSONString = "{\r\n   \"qId\": \"12345\", \r\n \"category\": \"1\",\r\n    \"correctAnswerIds\": \"Yes\",\r\n    \"question\": \"Is Java Object Oriented ?\",\r\n    \"weightage\": \"10\",\r\n    \"answers\": {\r\n        \"a\": \"Yes\",\r\n        \"b\": \"No\",\r\n        \"c\": \"Don't Know\",\r\n        \"d\": \"May Be\"\r\n    }\r\n}";
	String updatedquestionJSONString = "{\r\n  \"qId\": \"12345\", \r\n  \"category\": \"2\",\r\n    \"correctAnswerIds\": \"No\",\r\n    \"question\": \"Is C Object Oriented ?\",\r\n    \"weightage\": \"10\",\r\n    \"answers\": {\r\n        \"a\": \"No\",\r\n        \"b\": \"Why Not\",\r\n        \"c\": \"Don't Know\",\r\n        \"d\": \"May Be\"\r\n    }\r\n}";
	JSONObject result;
	Question question = new Question();
	List<Answer> answers;

	@Before
	public void setUp() {
		result = new JSONObject();
		questionHandler = new QuestionHandler();
		questionDAO = EasyMock.createMock(QuestionDAOImpl.class);
		answerDAO = EasyMock.createMock(AnswerDAOImpl.class);
		questionHandler.setQuestionDAO(questionDAO);
		try {

			questionDAO.addQuestion(EasyMock.anyObject(Question.class));
			EasyMock.expectLastCall().anyTimes();
			questionDAO.deleteQuestion(EasyMock.anyObject(Question.class));
			EasyMock.expectLastCall().anyTimes();
			questionDAO.updateQuestion(EasyMock.anyObject(Question.class));
			EasyMock.expectLastCall().anyTimes();
			EasyMock.expect(questionDAO.getQuestion(question.getQId()))
					.andReturn(new Question()).anyTimes();
			EasyMock.replay(questionDAO);

			answers = EasyMock.<List<Answer>> anyObject();

			answerDAO.addAnswer(answers);
			EasyMock.expectLastCall().anyTimes();
			EasyMock.expect(
					answerDAO.getAnswersByQuestion(EasyMock
							.anyObject(Question.class))).andReturn(answers)
					.anyTimes();
			answerDAO.deleteAnswer(EasyMock.anyObject(Answer.class));
			EasyMock.expectLastCall().anyTimes();
			EasyMock.replay(answerDAO);

			questionHandler.setAnswerDAO(answerDAO);
		} catch (DatabaseException e) {
			LOG.error("Error in testDAO {}", e);
			e.printStackTrace();
		}
	}

	@After
	public void tearDown() {
		questionHandler = null;
	}

	@Test
	public void testAddQuestion() {
		try {
			result = questionHandler.addQuestion(questionJSONString);
			Assert.assertEquals(Constants.SUCCESSFULLY_ADDED_QUESTION,
					result.get(Constants.RESULT));
		} catch (JSONException ex) {
			Assert.fail(Constants.WRONG_JSON_RETURNED + ex);
			ex.printStackTrace();
		} catch (HandlerException ex) {
			Assert.fail(" Error in adding Question." + ex);
			ex.printStackTrace();
		}
	}

	//@Test
	public void removeQuestionTest() {
		try {
			result = questionHandler.removeQuestion(String.valueOf(question
					.getQId()));
			Assert.assertEquals(Constants.SUCCESSFULLY_REMOVED_QUESTION,
					result.get(Constants.RESULT));
		} catch (JSONException ex) {
			Assert.fail(Constants.WRONG_JSON_RETURNED + ex);
		} catch (HandlerException ex) {
			Assert.fail(" Error in Removing Question." + ex);
		}
	}

	@Test
	public void updateQuestionTest() {
		try {
			result = questionHandler.updateQuestion(updatedquestionJSONString);
			Assert.assertEquals(Constants.SUCCESSFULLY_UPDATED_QUESTION,
					result.get(Constants.RESULT));
		} catch (JSONException ex) {
			Assert.fail(Constants.WRONG_JSON_RETURNED + ex);
		} catch (HandlerException ex) {
			Assert.fail(" Error in Updating Question." + ex);
		}
	}

	@Test
	public void getQuestionByIdTest() {
		try {
			result = questionHandler.getQuestionById(String.valueOf(question
					.getQId()));
			Assert.assertEquals(Constants.SUCCESSFULLY_GOT_QUESTION_BY_ID,
					result.get(Constants.RESULT));
		} catch (JSONException ex) {
			Assert.fail(Constants.WRONG_JSON_RETURNED + ex);
		} catch (HandlerException ex) {
			Assert.fail(" Error in getting Question by Id." + ex);
		}
	}

}
