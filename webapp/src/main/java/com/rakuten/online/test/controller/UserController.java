package com.rakuten.online.test.controller;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.rakuten.online.test.business.logic.admin.UserHandler;
import com.rakuten.online.test.commons.exception.HandlerException;
import com.rakuten.online.test.commons.util.Constants;

@RestController
public class UserController {

    @Autowired
    UserHandler userHandler;

    private static final Logger LOG = LoggerFactory
            .getLogger(UserController.class);

    @ResponseBody
    @RequestMapping(value = "/admin/user", method = RequestMethod.POST)
    public ResponseEntity<String> addUser(@RequestBody String json) {
        LOG.debug("Adding user to the question bank application");
        try {
            JSONObject returnObject = this.getUserHandler().addUser(json);
            return new ResponseEntity<String>(returnObject.toString(),
                    HttpStatus.OK);
        } catch (HandlerException e) {
            LOG.error(Constants.ERROR_IN_HANDLER, e);
            return new ResponseEntity<String>(Constants.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/admin/user/{mailId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteUser(
            @PathVariable("mailId") String mailId) {
        LOG.debug("Removing user from the question bank application");
        try {
            JSONObject returnObject = this.getUserHandler().removeUser(
                    this.getEmailId(mailId));
            return new ResponseEntity<String>(returnObject.toString(),
                    HttpStatus.OK);
        } catch (HandlerException e) {
            LOG.error(Constants.ERROR_IN_HANDLER, e);
            return new ResponseEntity<String>(Constants.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/admin/user", method = RequestMethod.PUT)
    public ResponseEntity<String> updateUser(@RequestBody String json) {
        LOG.debug("Updating user in the question bank application");
        try {
            JSONObject returnObject = this.getUserHandler().updateUser(json);
            return new ResponseEntity<String>(returnObject.toString(),
                    HttpStatus.OK);
        } catch (HandlerException e) {
            LOG.error(Constants.ERROR_IN_HANDLER, e);
            return new ResponseEntity<String>(Constants.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/admin/user/{mailId}", method = RequestMethod.GET)
    public ResponseEntity<String> getSingleUser(
            @PathVariable("mailId") String mailId) {
        LOG.debug("Gettign a single user for mail Id {}", mailId);
        try {
            JSONObject returnObject = this.getUserHandler().getSingleUser(
                    this.getEmailId(mailId));
            return new ResponseEntity<String>(returnObject.toString(),
                    HttpStatus.OK);
        } catch (HandlerException e) {
            LOG.error(Constants.ERROR_IN_HANDLER, e);
            return new ResponseEntity<String>(Constants.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/admin/user/", method = RequestMethod.GET)
    public ResponseEntity<String> getAllUsers() {
        LOG.debug("Get all users in question bank");
        try {
            JSONObject returnObject = this.getUserHandler().getAllUsers();
            return new ResponseEntity<String>(returnObject.toString(),
                    HttpStatus.OK);
        } catch (HandlerException e) {
            LOG.error(Constants.ERROR_IN_HANDLER, e);
            return new ResponseEntity<String>(Constants.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private String getEmailId(String mailId) {
        StringBuilder emailId = new StringBuilder(mailId);
        emailId.append(new StringBuilder(Constants.COM));
        return emailId.toString();
    }

    public UserHandler getUserHandler() {
        return this.userHandler;
    }

    public void setUserHandler(UserHandler userHandler) {
        this.userHandler = userHandler;
    }

}
