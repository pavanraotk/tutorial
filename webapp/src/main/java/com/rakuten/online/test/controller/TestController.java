package com.rakuten.online.test.controller;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rakuten.online.test.business.logic.admin.TestHandler;
import com.rakuten.online.test.commons.exception.HandlerException;
import com.rakuten.online.test.commons.util.Constants;

@RestController
public class TestController {

	@Autowired
	TestHandler testHandler;
	private static final Logger LOG = LoggerFactory
			.getLogger(TestController.class);

	@RequestMapping(value = "/admin/test", method = RequestMethod.POST)
	public ResponseEntity<String> addTest(@RequestBody String testJson) {
		LOG.debug("Adding test to the question bank application.");
		try {
			JSONObject returnObject = this.getTestHandler().addTest(testJson);
			return new ResponseEntity<String>(returnObject.toString(),
					HttpStatus.OK);
		} catch (HandlerException e) {
			LOG.error(Constants.ERROR_IN_HANDLER, e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/admin/test/{testId}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteTest(
			@PathVariable("testId") String testId) {
		LOG.debug("Removing test from the question bank application from given testId.");
		try {
			JSONObject returnObject = this.getTestHandler().removeTest(testId);
			return new ResponseEntity<String>(returnObject.toString(),
					HttpStatus.OK);
		} catch (HandlerException e) {
			LOG.error(Constants.ERROR_IN_HANDLER, e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/admin/test", method = RequestMethod.PUT)
	public ResponseEntity<String> updateTest(
			@RequestBody String updatedTestJsonObject) {
		LOG.debug("Updating test in the question bank application from given updated JSON.");
		try {
			JSONObject returnObject = this.getTestHandler().updateTest(
					updatedTestJsonObject);
			return new ResponseEntity<String>(returnObject.toString(),
					HttpStatus.OK);
		} catch (HandlerException e) {
			LOG.error(Constants.ERROR_IN_HANDLER, e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/admin/test/{testId}", method = RequestMethod.GET)
	public ResponseEntity<String> getTestByTestId(
			@PathVariable("testId") String testId) {
		LOG.debug("Getting test from the question bank application from given testId.");
		try {
			JSONObject returnObject = this.getTestHandler().getTestByTestId(
					testId);
			return new ResponseEntity<String>(returnObject.toString(),
					HttpStatus.OK);
		} catch (HandlerException e) {
			LOG.error(Constants.ERROR_IN_HANDLER, e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/admin/test/category/{category}", method = RequestMethod.GET)
	public ResponseEntity<String> getTestsByCategory(
			@PathVariable("category") String category) {
		LOG.debug("Getting test from the question bank application from given category.");
		try {
			JSONObject returnObject = this.getTestHandler().getTestsByCategory(
					category);
			return new ResponseEntity<String>(returnObject.toString(),
					HttpStatus.OK);
		} catch (HandlerException e) {
			LOG.error(Constants.ERROR_IN_HANDLER, e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/admin/test/experience/{experience}", method = RequestMethod.GET)
	public ResponseEntity<String> getTestsByExperience(
			@PathVariable("experience") String experience) {
		LOG.debug("Getting test from the question bank application from given experience.");
		try {
			JSONObject returnObject = this.getTestHandler()
					.getTestsByExperience(experience);
			return new ResponseEntity<String>(returnObject.toString(),
					HttpStatus.OK);
		} catch (HandlerException e) {
			LOG.error(Constants.ERROR_IN_HANDLER, e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/admin/test/scheduleTest/{userEmailId}", method = RequestMethod.GET)
	public ResponseEntity<String> scheduleTestForUser(
			@PathVariable("userEmailId") String userEmailId) {
		LOG.debug("Scheduling the test for user based on experience.");
		try {
			JSONObject returnObject = this.getTestHandler().scheduleTest(
					this.getEmailId(userEmailId));
			return new ResponseEntity<String>(returnObject.toString(),
					HttpStatus.OK);
		} catch (HandlerException e) {
			LOG.error(Constants.ERROR_IN_HANDLER, e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private String getEmailId(String mailId) {
		StringBuilder emailId = new StringBuilder(mailId);
		emailId.append(new StringBuilder(Constants.COM));
		return emailId.toString();
	}

	public TestHandler getTestHandler() {
		return testHandler;
	}

	public void setTestHandler(TestHandler testHandler) {
		this.testHandler = testHandler;
	}

}
