package com.rakuten.online.test.controller;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rakuten.online.test.business.logic.admin.QuestionHandler;
import com.rakuten.online.test.commons.exception.HandlerException;
import com.rakuten.online.test.commons.util.Constants;

@RestController
public class QuestionController {

    @Autowired
    QuestionHandler questionHandler;
    private static final Logger LOG = LoggerFactory
            .getLogger(QuestionController.class);

    @RequestMapping(value = "/admin/question", method = RequestMethod.POST)
    public ResponseEntity<String> addQuestion(@RequestBody String questionJson) {
        LOG.debug("Adding question to the question bank application.");
        try {
            JSONObject returnObject = this.getQuestionHandler().addQuestion(
                    questionJson);
            return new ResponseEntity<String>(returnObject.toString(),
                    HttpStatus.OK);
        } catch (HandlerException e) {
            LOG.error(Constants.ERROR_IN_HANDLER, e);
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/admin/question/{questionId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteQuestion(
            @PathVariable("questionId") String questionId) {
        LOG.debug("Removing question from the question bank application from given questionId.");
        try {
            JSONObject returnObject = this.getQuestionHandler().removeQuestion(
                    questionId);
            return new ResponseEntity<String>(returnObject.toString(),
                    HttpStatus.OK);
        } catch (HandlerException e) {
            LOG.error(Constants.ERROR_IN_HANDLER, e);
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/admin/question", method = RequestMethod.PUT)
    public ResponseEntity<String> updateQuestion(
            @RequestBody String updatedQuestionJsonObject) {
        LOG.debug("Updating question in the question bank application from given updated JSON.");
        try {
            JSONObject returnObject = this.getQuestionHandler().updateQuestion(
                    updatedQuestionJsonObject);
            return new ResponseEntity<String>(returnObject.toString(),
                    HttpStatus.OK);
        } catch (HandlerException e) {
            LOG.error(Constants.ERROR_IN_HANDLER, e);
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/admin/question/{questionId}", method = RequestMethod.GET)
    public ResponseEntity<String> getQuestionByQuestionId(
            @PathVariable("questionId") String questionId) {
        LOG.debug("Getting question from the question bank application from given questionId.");
        try {
            JSONObject returnObject = this.getQuestionHandler()
                    .getQuestionById(questionId);
            return new ResponseEntity<String>(returnObject.toString(),
                    HttpStatus.OK);
        } catch (HandlerException e) {
            LOG.error(Constants.ERROR_IN_HANDLER, e);
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private QuestionHandler getQuestionHandler() {
        return questionHandler;
    }

    public void setQuestionHandler(QuestionHandler questionHandler) {
        this.questionHandler = questionHandler;
    }
}
