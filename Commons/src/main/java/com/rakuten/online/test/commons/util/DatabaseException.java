package com.rakuten.online.test.commons.util;

public class DatabaseException extends Exception {

    /**
     * Serializing
     */
    private static final long serialVersionUID = 1L;
    private static final String MESSAGE = null;

    public DatabaseException() {
        super();
    }

    public DatabaseException(String exception) {
        super(exception);
    }

    public DatabaseException(Throwable cause) {
        super(cause);
    }

    @Override
    public String toString() {
        return MESSAGE;
    }

    @Override
    public String getMessage() {
        return MESSAGE;
    }

}
