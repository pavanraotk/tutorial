package com.rakuten.online.test.commons.util;

public class Constants {

    public static final String NAME = "name";
    public static final String ISADMIN = "isAdmin";
    public static final String FIELD = "password";
    public static final String EXPERIENCE = "experience";
    public static final String MAIL = "mail";
    public static final String PHONE = "phone";
    public static final String ID = "id";

    public static final String TEST = "test";
    public static final String TEST_LIST = "testList";
    public static final String SCHEDULED_TEST = "scheduledTest";
    public static final String SUCCESSFULLY_ADDED_TEST = "Successfully added test";
    public static final String SUCCESSFULLY_REMOVED_TEST = "Successfully removed test";
    public static final String SUCCESSFULLY_UPDATED_TEST = "Successfully updated test";
    public static final String SUCCESSFULLY_GOT_TEST_BY_CATEGORY = "Successfully got the test Lists";
    public static final String SUCCESSFULLY_GOT_TEST_BY_ID = "Successfully got the test by Test Id";
    public static final String SUCCESSFULLY_GOT_TEST_BY_EXPERIENCE = "Successfully got the test by experience";
    public static final String SUCCESSFULLY_SCHEDULED_TEST = "Successfully scheduled test";
    public static final String SUCCESSFULLY_ADDED_QUESTION = "Successfully added the Question";
    public static final String SUCCESSFULLY_REMOVED_QUESTION = "Successfully removed Question";
    public static final String SUCCESSFULLY_UPDATED_QUESTION = "Successfully updated Question";
    public static final String SUCCESSFULLY_GOT_QUESTION_BY_ID = "Successfully got the question by Question Id";
   
    public static final String RESULT = "Result";

    public static final String SUCCESSFULLY_ADDED_USER = "Successfully added user";
    public static final String SUCCESSFULLY_REMOVED_USER = "Successfully removed user";
    public static final String SUCCESSFULLY_UPDATED_USER = "Successfully updated user";

    public static final String INTERNAL_SERVER_ERROR = "Internal Server Error";

    public static final String COM = ".com";

    public static final String ERROR_MESSAGE = "Standard handler exception";

    public static final String ERROR_IN_HANDLER = "Error in handler {}";

    public static final String WITH_EXCEPTION = " with exception ";
    public static final String QUESTIONS = "questions";
    public static final String QUESTION = "question";
    public static final String ANSWERS = "answers";

    public static final String WRONG_JSON_RETURNED = "Wrong json returned ";
    public static final String ERROR_IN_GETTING_TEST = " Error in getting test ";
    public static final String CATEGORY ="category";
    public static final String DURATION ="duration";
    public static final String TESTID ="testId";
    public static final String CORRECTANSWERIDS ="correctAnswerIds";
    public static final String WEIGHTAGE ="weightage";
    public static final String QID="qId";
    
    private Constants() {

    }

}
