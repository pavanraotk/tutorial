package com.rakuten.online.test.commons.util;

public class DatabaseConstants {

	public static final String ANSWER_TABLE = "ANSWER";
	public static final String ANSWER = "ANS";
	public static final String ANSWER_ID = "A_ID";
	public static final String FIND_ALL_ANSWERS = "Answer.findAll";
	public static final String FIND_ANSWER_BY_QUESTION = "Anser.findAnswerByQuestion";
	public static final int LENGTH_OF_ANSWER = 200;

	public static final String QUESTION_TABLE = "QUESTION";
	public static final String QUESTION_ID = "Q_ID";
	public static final String CATEGORY = "CATEGORY";
	public static final String CORRECTANSWERIDS = "CORRECT_ANSWER_IDS";
	public static final String QUESTION = "QUESTION";
	public static final String WEIGTHAGE = "WEIGHTAGE";
	public static final String FIND_ALL_QUESTIONS = "Question.findAll";
	public static final String MAPPED_QUESTION = "question";
	public static final String MAPPED_BY_QUESTIONS = "questions";
	public static final int LENGTH_OF_CORRECTANSWERIDS = 20;
	public static final int LENGTH_OF_QUESTION = 255;

	public static final String SESSIONINFO_TABLE = "SESSION_INFO";
	public static final String FIND_ALL_SESSIONINFO = "SessionInfo.findAll";
	public static final String SESSION_ID = "SESSION_ID";
	public static final String INFO = "INFO";
	public static final String TIME_TAKEN = "TIME_TAKEN";
	public static final String MAPPED_SESSIONINFO = "sessionInfo";
	public static final int LENGTH_OF_INFO = 30;

	public static final String TEST_TABLE = "TEST";
	public static final String FIND_ALL_TESTS = "Test.findAll";
	public static final String FIND_ALL_TESTS_BY_CATEGORY = "Test.findAllByCategory";
	public static final String FIND_ALL_TESTS_BY_EXPERIENCE = "Test.findAllByExperience";
	public static final String TEST_ID = "TEST_ID";
	public static final String DURATION = "DURATION";
	public static final String EXPERIENCE = "EXPERIENCE";
	public static final String QUESTIONIDS = "Q_IDS";
	public static final String MAPPED_TEST = "test";
	public static final int LENGTH_OF_QUESTIONIDS = 255;

	public static final String USER_TABLE = "USERS";
	public static final String FIND_ALL_USERS = "User.findAll";
	public static final String USERID = "ID";
	public static final String ISADMIN = "IS_ADMIN";
	public static final String NAME = "NAME";
	public static final String FIELD = "PASSWORD";
	public static final String MAPPED_USER = "user";

	public static final String USERINFO_TABLE = "USERINFO";
	public static final String FIND_ALL_USERINFO = "UserInfo.findAll";
	public static final String FIND_USERINFO_FROM_USER = "UserInfo.findFromUser";
	public static final String FIND_USERINFO_FROM_EMAILID = "UserInfo.findFromEmailID";
	public static final String GUID = "GUID";
	public static final String MAIL = "MAIL";
	public static final String PHONE = "PHONE";
	public static final int LENGTH_OF_MAIL = 30;
	public static final int LENGTH_OF_PHONE = 20;
	public static final int LENGTH_OF_PASSWORD = 30;
	public static final int LENGTH_OF_NAME = 20;

	public static final String USERTESTHISTORY_TABLE = "USERTESTHISTORY";
	public static final String FIND_ALL_USERTESTHISTORY = "UserTestHistory.findAll";
	public static final String INPUT_ANSWER_IDS = "INPUT_ANSWER_IDS";
	public static final int LENGTH_OF_INPUTANSWERIDS = 30;

	public static final String TESTQUESTIONS = "TESTQUESTIONS";
	public static final String TESTID = "TESTID";
	public static final String QUESTIONID = "QUESTIONID";

	public static final String WITH_EXCEPTION = " with exception ";

	public static final String EXPECTED_ERROR = "Expected error";

	private DatabaseConstants() {

	}

}
