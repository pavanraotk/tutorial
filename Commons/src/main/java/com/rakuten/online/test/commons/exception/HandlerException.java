package com.rakuten.online.test.commons.exception;

import com.rakuten.online.test.commons.util.Constants;

public class HandlerException extends Exception {

    private static final long serialVersionUID = 1L;
    private static final String MESSAGE = Constants.ERROR_MESSAGE;

    public HandlerException() {
        super();
    }

    public HandlerException(String message) {
        super(message);
    }

    public HandlerException(Throwable cause) {
        super(cause);
    }

    @Override
    public String toString() {
        return MESSAGE;
    }

    @Override
    public String getMessage() {
        return MESSAGE;
    }

}
