package com.rakuten.online.test.persistence.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;

import com.rakuten.online.test.commons.util.DatabaseConstants;
import com.rakuten.online.test.commons.util.DatabaseException;
import com.rakuten.online.test.persistence.Answer;
import com.rakuten.online.test.persistence.Question;

@ContextConfiguration(locations = { "classpath:src/test/resources/META-INF/persistence.xml" })
public class TestDAOImplTest {

    private static final Logger LOG = LoggerFactory
            .getLogger(UserDAOImplTest.class);

    TestDAOImpl testDAOImpl;
    com.rakuten.online.test.persistence.Test test, returnTest;
    int testId, category, duration;
    BigDecimal experience;
    String qIds;
    QuestionDAOImpl questionDaoImpl;
    List<Question> qList = new ArrayList<Question>();
    Question q1, q2;

    @Before
    public void setUp() throws DatabaseException {
        testDAOImpl = new TestDAOImpl();
        test = new com.rakuten.online.test.persistence.Test();

        testId = 12345;
        category = 2;
        experience = new BigDecimal(2);
        duration = 30;
        q1 = new Question();
        q2 = new Question();
        questionDaoImpl = new QuestionDAOImpl();
        q1.setQuestion("how are you?");
        Answer a1 = new Answer();
        Answer a2 = new Answer();
        a1.setAns("Yes");
        a1.setQuestion(q1);
        a2.setAns("No");
        a2.setQuestion(q1);
        List<Answer> answerList= new ArrayList<Answer>();
        answerList.add(a1);
        answerList.add(a2);
        q1.setAnswers(answerList);
        q1.setCorrectAnswerIds("Fine");
        // q1.setQId(1);
        q2.setQuestion("Who are you?");
        q2.setCorrectAnswerIds("Human");
        Answer b1 = new Answer();
        Answer b2 = new Answer();
        b1.setAns("Yes");
        b1.setQuestion(q2);
        b2.setAns("No");
        b2.setQuestion(q2);
        List<Answer> answerList2= new ArrayList<Answer>();
        answerList2.add(b1);
        answerList2.add(b2);
        
        q2.setAnswers(answerList2);
         // q2.setQId(2);
//        questionDaoImpl.addQuestion(q1);
//        questionDaoImpl.addQuestion(q2);

        qList.add(q1);
        qList.add(q2);


        test.setTestId(testId);
        test.setCategory(category);
        test.setExperience(experience);
        test.setDuration(duration);
        test.setQuestions(qList);

    }

    @After
    public void tearDown() {
        testDAOImpl = null;
        try {
            questionDaoImpl.deleteQuestion(q1);
            questionDaoImpl.deleteQuestion(q2);
        } catch (DatabaseException e) {
            LOG.error("Error in deleting question {}", e);
        }

    }

    @Test
    public void test() {
        try {

            testDAOImpl.addTest(test);
            returnTest = testDAOImpl.getTest(testId);
            Assert.assertEquals(test.getTestId(), returnTest.getTestId());

            test.setDuration(60);
            testDAOImpl.updateTest(test);
            returnTest = testDAOImpl.getTest(testId);
            Assert.assertEquals(test.getDuration(), returnTest.getDuration());

            testDAOImpl.deleteTest(test);
            returnTest = testDAOImpl.getTest(testId);
            Assert.assertEquals(null, returnTest);

        } catch (Exception ex) {
            LOG.error("Error in db interaction {}", ex);
            ex.printStackTrace();
            Assert.fail("Error in db interaction " + ex);
        }
    }

    @Test
    public void testCoverBranchUpdate() {
        try {
            testDAOImpl.updateTest(test);
        } catch (DatabaseException ex) {
            LOG.info(DatabaseConstants.EXPECTED_ERROR);
        }
    }

    @Test
    public void testCoverBranchDelete() {
        try {
            testDAOImpl.deleteTest(test);
        } catch (DatabaseException ex) {
            LOG.info(DatabaseConstants.EXPECTED_ERROR);
        }
    }

}
