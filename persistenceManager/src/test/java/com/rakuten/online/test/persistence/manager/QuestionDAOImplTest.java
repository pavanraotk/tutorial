package com.rakuten.online.test.persistence.manager;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;

import com.rakuten.online.test.commons.util.DatabaseConstants;
import com.rakuten.online.test.commons.util.DatabaseException;
import com.rakuten.online.test.persistence.Answer;
import com.rakuten.online.test.persistence.Question;

@ContextConfiguration(locations = { "classpath:src/test/resources/META-INF/persistence.xml" })
public class QuestionDAOImplTest {

    private static final Logger LOG = LoggerFactory
            .getLogger(QuestionDAOImplTest.class);

    Question question;
    List<Question> returnQuestions;
    QuestionDAOImpl questionDAO;
    Answer answer1, answer2, answer3;
    List<Answer> answers;

    String actualAnswer1, actualAnswer2, actualAnswer3;
    int weightage, category;
    String questionString, correctAnswerIds;

    @Before
    public void setUp() {
        question = new Question();
        questionDAO = new QuestionDAOImpl();

        weightage = 10;
        questionString = "Who is PM of UK?";

        answer1 = new Answer();
        answer2 = new Answer();
        answer3 = new Answer();

        answers = new ArrayList<>();

        actualAnswer1 = "David Cameron";
        actualAnswer2 = "Tony Blair";
        actualAnswer3 = "No PM, only queen!";

        answer1.setAns(actualAnswer1);
        answer2.setAns(actualAnswer2);
        answer3.setAns(actualAnswer3);

        answers.add(answer1);
        answers.add(answer2);
        answers.add(answer3);

        correctAnswerIds = "David Cameron";

        category = 10;

        question.setQuestion(questionString);
        question.setWeightage(weightage);
        question.setCorrectAnswerIds(correctAnswerIds);
        question.setAnswers(answers);
    }

    @After
    public void tearDown() {
        question = null;
        questionDAO = null;
    }

    @Test
    public void test() {
        try {
            questionDAO.addQuestion(question);
            returnQuestions = questionDAO.getAllQuestions();
            for (Question returnQuestion : returnQuestions)
                Assert.assertEquals(question.getQuestion(),
                        returnQuestion.getQuestion());
            question.setCategory(category);
            questionDAO.updateQuestion(question);
            returnQuestions = questionDAO.getAllQuestions();
            for (Question returnQuestion : returnQuestions)
                Assert.assertEquals(question.getCategory(),
                        returnQuestion.getCategory());
            questionDAO.deleteQuestion(question);
            returnQuestions = questionDAO.getAllQuestions();
            Assert.assertEquals(0, returnQuestions.size());
            questionDAO.updateQuestion(question);
        } catch (DatabaseException ex) {
            LOG.info(DatabaseConstants.EXPECTED_ERROR);
        } catch (Exception ex) {
            Assert.fail("Failed DB interaction " + ex);
        }
    }

}
