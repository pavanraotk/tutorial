package com.rakuten.online.test.persistence.manager;

import java.math.BigDecimal;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;

import com.rakuten.online.test.commons.util.DatabaseConstants;
import com.rakuten.online.test.commons.util.DatabaseException;
import com.rakuten.online.test.persistence.User;
import com.rakuten.online.test.persistence.UserInfo;

@ContextConfiguration(locations = { "classpath:src/test/resources/META-INF/persistence.xml" })
public class UserInfoDAOImplTest {

    private static final Logger LOG = LoggerFactory
            .getLogger(UserInfoDAOImplTest.class);

    UserInfoDAOImpl userInfoDAO;
    UserDAOImpl userDAO;
    User user;
    UserInfo userInfo, returnUserInfo;

    int id;
    byte isAdmin;
    String name, password, mail, phone;
    BigDecimal experience, newExperience;

    @Before
    public void setUp() {

        id = 12345;
        isAdmin = 0;
        name = "Junit";
        password = "JunitPassword";
        mail = "Junit@rakuten.com";
        phone = "+919127898799";
        experience = new BigDecimal(1.0);
        newExperience = new BigDecimal(2.0);

        userInfoDAO = new UserInfoDAOImpl();
        userDAO = new UserDAOImpl();
        user = new User();
        userInfo = new UserInfo();

        user.setId(id);
        user.setIsAdmin(isAdmin);
        user.setName(name);
        user.setPassword(password);

        userInfo.setExperience(experience);
        userInfo.setMail(mail);
        userInfo.setPhone(phone);
        userInfo.setUser(user);

    }

    @After
    public void tearDown() {
        try {
            userDAO.deleteUser(user);
        } catch (Exception ex) {
            LOG.error("Error in deleting user {}", ex);
        }
        userDAO = null;
        userInfoDAO = null;
        user = null;
        userInfo = null;
    }

    @Test
    public void test() {
        try {
            userDAO.addUser(user);
            userInfoDAO.addUserInfo(userInfo);
            returnUserInfo = userInfoDAO.getUser(user);
            Assert.assertEquals(userInfo.getMail(), returnUserInfo.getMail());
            userInfo.setExperience(newExperience);
            userInfoDAO.updateUserInfo(userInfo);
            returnUserInfo = userInfoDAO.getUser(user);
            if (returnUserInfo.getExperience().compareTo(
                    userInfo.getExperience()) != 0)
                Assert.fail("Error in updating the user info object");
            returnUserInfo = userInfoDAO.getUserFromEmail(mail);
            Assert.assertEquals(name, returnUserInfo.getUser().getName());
            userInfoDAO.deleteUser(userInfo);
            
        } catch (DatabaseException ex) {
            LOG.info(DatabaseConstants.EXPECTED_ERROR);
        } catch (Exception ex) {
            LOG.error("Error in database interaction {}", ex);
            Assert.fail("Error in database interaction " + ex);
        }
    }

    @Test
    public void coveringBranchesUpdate() {
        try {
            userInfoDAO.updateUserInfo(userInfo);
        } catch (DatabaseException ex) {
            LOG.info(DatabaseConstants.EXPECTED_ERROR);
        }
    }

    @Test
    public void coveringBranchesDelete() {
        try {
            userInfoDAO.deleteUser(userInfo);
        } catch (DatabaseException ex) {
            LOG.info(DatabaseConstants.EXPECTED_ERROR);
        }
    }

}
