package com.rakuten.online.test.persistence.manager;

import java.util.List;

import javax.persistence.EntityManager;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;

import com.rakuten.online.test.commons.util.DatabaseConstants;
import com.rakuten.online.test.commons.util.DatabaseException;
import com.rakuten.online.test.persistence.User;

@ContextConfiguration(locations = { "classpath:src/test/resources/META-INF/persistence.xml" })
public class UserDAOImplTest {

    private static final Logger LOG = LoggerFactory
            .getLogger(UserDAOImplTest.class);

    UserDAOImpl userDAOImpl;
    User user, returnUser;
    int id;
    byte isAdmin;
    String name, password, newPassword;
    List<User> users;

    @Before
    public void setUp() {
        userDAOImpl = new UserDAOImpl();
        user = new User();

        id = 12345;
        isAdmin = 0;
        name = "Junit";
        password = "Password";
        newPassword = "Password2";

        user.setId(id);
        user.setIsAdmin(isAdmin);
        user.setName(name);
        user.setPassword(password);
    }

    @After
    public void tearDown() {
        userDAOImpl = null;
    }

    @Test
    public void test() {
        try {
            userDAOImpl.addUser(user);
            returnUser = userDAOImpl.getUser(id);
            Assert.assertEquals(user.getName(), returnUser.getName());
            user.setPassword(newPassword);
            userDAOImpl.updateUser(user);
            returnUser = userDAOImpl.getUser(id);
            Assert.assertEquals(user.getPassword(), returnUser.getPassword());
            users = userDAOImpl.getAllUsers();
            Assert.assertEquals(user.getPassword(), users.get(0).getPassword());
            userDAOImpl.deleteUser(user);
            returnUser = userDAOImpl.getUser(id);
            Assert.assertEquals(null, returnUser);
        } catch (Exception ex) {
            LOG.error("Error in db interaction {}", ex);
            ex.printStackTrace();
            Assert.fail("Error in db interaction " + ex);
        }
    }

    @Test
    public void testEM() {
        EntityManager em = userDAOImpl.getEntityManager();
        em.close();
        userDAOImpl.closeEntityManager();
        em = null;
        userDAOImpl.closeEntityManager();
    }

    @Test
    public void testCoverBranchUpdate() {
        try {
            userDAOImpl.updateUser(user);
        } catch (DatabaseException ex) {
            LOG.info(DatabaseConstants.EXPECTED_ERROR);
        }
    }

    @Test
    public void testCoverBranchDelete() {
        try {
            userDAOImpl.deleteUser(user);
        } catch (DatabaseException ex) {
            LOG.info(DatabaseConstants.EXPECTED_ERROR);
        }
    }

}
