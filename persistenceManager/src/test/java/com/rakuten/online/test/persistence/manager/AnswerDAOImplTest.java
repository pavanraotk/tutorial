package com.rakuten.online.test.persistence.manager;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.rakuten.online.test.commons.util.DatabaseException;
import com.rakuten.online.test.persistence.Answer;
import com.rakuten.online.test.persistence.Question;

@ContextConfiguration(locations = { "classpath:src/test/resources/META-INF/persistence.xml" })
public class AnswerDAOImplTest {

	Question question;

	AnswerDAOImpl answerDAO;

	QuestionDAOImpl questionDAO;

	Answer answer1, answer2, answer3, answer4;

	List<Answer> answers, returnAnswers;

	@Before
	public void setUp() {
		answerDAO = new AnswerDAOImpl();
		questionDAO = new QuestionDAOImpl();
		answers = new ArrayList<Answer>();

		question = new Question();

		answer1 = answer2 = answer3 = answer4 = new Answer();

		question.setQuestion("Which is Japan's biggest e-commerece site?");
		question.setCategory(1);
		question.setWeightage(1);

		try {
			questionDAO.addQuestion(question);
		} catch (DatabaseException ex) {
			ex.printStackTrace();
		}
		answer1.setAns("Rakuten");
		answer1.setQuestion(question);

		answer2.setAns("Amazon");
		answer2.setQuestion(question);

		answer3.setAns("Flipkart");
		answer3.setQuestion(question);

		answer4.setAns("Not sure");
		answer4.setQuestion(question);

		answers.add(answer1);
		answers.add(answer2);
		answers.add(answer3);
		answers.add(answer4);
	}

	@Test
	public void test() {
		try {
			answerDAO.addAnswer(answers);
			returnAnswers = answerDAO.getAnswersByQuestion(question);
			Assert.assertNotNull(returnAnswers);
			answer4.setAns("Japan doesn't allow ecommerce");
			answerDAO.updateAnswer(answer4);
			answerDAO.deleteAnswer(answer1);
			answerDAO.deleteAnswer(answer2);
			answerDAO.deleteAnswer(answer3);
			answerDAO.deleteAnswer(answer4);
			returnAnswers = answerDAO.getAnswersByQuestion(question);
			Assert.assertEquals(0, returnAnswers.size());
		} catch (DatabaseException ex) {
			Assert.fail("Error in database interaction");
		}
	}

}
