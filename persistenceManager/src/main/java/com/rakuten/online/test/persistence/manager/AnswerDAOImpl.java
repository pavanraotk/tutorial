package com.rakuten.online.test.persistence.manager;

import java.util.List;

import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

import com.rakuten.online.test.commons.util.DatabaseConstants;
import com.rakuten.online.test.commons.util.DatabaseException;
import com.rakuten.online.test.persistence.Answer;
import com.rakuten.online.test.persistence.Question;

public class AnswerDAOImpl extends AbstractEntityManager {

	private static final Logger LOG = Logger.getLogger(AnswerDAOImpl.class);

	public void addAnswer(List<Answer> answers) throws DatabaseException {
		LOG.debug("Adding list of answers");
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			for (Answer answer : answers)
				em.persist(answer);
			em.getTransaction().commit();
		} catch (Exception ex) {
			LOG.error("Error in adding list of answers to database {}", ex);
			throw new DatabaseException(
					"Error in adding list of answers to database {}" + ex);
		} finally {
			closeEntityManager();
		}
	}

	public void updateAnswer(Answer answer) throws DatabaseException {
		LOG.debug("Update answer");
		try {
			em = getEntityManager();
			Answer answerToBeUpdated = em.find(Answer.class, answer.getAId());
			if (answerToBeUpdated != null) {
				em.getTransaction().begin();
				answerToBeUpdated.setAns(answer.getAns());
				em.getTransaction().commit();
			}
		} catch (Exception ex) {
			LOG.error("Error in updating answer {}", ex);
			throw new DatabaseException("Error in updating answer " + ex);
		} finally {
			closeEntityManager();
		}
	}

	public void deleteAnswer(Answer answer) throws DatabaseException {
		LOG.debug("Delete answer");
		try {
			em = getEntityManager();
			Answer answerToBeDeleted = em.find(Answer.class, answer.getAId());
			if (answerToBeDeleted != null) {
				em.getTransaction().begin();
				em.remove(answerToBeDeleted);
				em.getTransaction().commit();
			}
		} catch (Exception ex) {
			LOG.error("Error in deleting answer {}", ex);
			throw new DatabaseException("Error in deleting answer " + ex);
		} finally {
			closeEntityManager();
		}
	}

	public List<Answer> getAnswersByQuestion(Question question)
			throws DatabaseException {
		List<Answer> answers = null;
		LOG.debug("Getting answers by question " + question.getQuestion());
		try {
			em = getEntityManager();
			TypedQuery<Answer> getAnswers = em.createNamedQuery(
					DatabaseConstants.FIND_ANSWER_BY_QUESTION, Answer.class);
			getAnswers.setParameter("question", question);
			answers = getAnswers.getResultList();
		} catch (Exception ex) {
			LOG.error("Error in getting answers by question {}", ex);
			throw new DatabaseException("Error in getting answers by question "
					+ ex);
		} finally {
			closeEntityManager();
		}
		return answers;
	}
}
