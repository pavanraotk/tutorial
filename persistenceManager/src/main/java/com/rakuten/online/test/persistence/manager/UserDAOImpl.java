package com.rakuten.online.test.persistence.manager;

import java.util.List;

import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rakuten.online.test.commons.util.DatabaseConstants;
import com.rakuten.online.test.commons.util.DatabaseException;
import com.rakuten.online.test.persistence.User;

public class UserDAOImpl extends AbstractEntityManager {

    private static final Logger LOG = LoggerFactory
            .getLogger(UserDAOImpl.class);

    /**
     * 
     * @param user
     *            User object is added to the user table. Unique constraint
     *            violation is caught in the exception
     */
    public void addUser(User user) throws DatabaseException {
        LOG.debug("Adding user to the user table");
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(user);
            em.getTransaction().commit();
            em.getEntityManagerFactory().getCache().evictAll();
        } catch (Exception ex) {
            LOG.error("Error in adding user to user table {}", ex);
            throw new DatabaseException("Error in adding user to user table "
                    + ex);
        } finally {
            closeEntityManager();
        }
    }

    /**
     * 
     * @param userId
     * @return user: Returns the user object for given ID.
     */
    public User getUser(int userId) throws DatabaseException {
        LOG.debug("Getting user for user {}", userId);
        User user = null;
        try {
            em = getEntityManager();
            user = em.find(User.class, userId);
        } catch (Exception ex) {
            LOG.error("Error in finding user {}", ex);
            throw new DatabaseException("Error in finding user " + ex);
        } finally {
            closeEntityManager();
        }
        return user;
    }

    /**
     * 
     * @param user
     *            Updates the user row in the table.
     */
    public void updateUser(User user) throws DatabaseException {
        LOG.debug("Update user table {}", user.getId());
        User updatingUser = new User();
        try {
            em = getEntityManager();
            updatingUser = em.find(User.class, user.getId());
            if (updatingUser != null) {
                em.getTransaction().begin();
                updatingUser.setIsAdmin(user.getIsAdmin());
                updatingUser.setName(user.getName());
                updatingUser.setPassword(user.getPassword());
                em.getTransaction().commit();
            }
        } catch (Exception ex) {
            LOG.error("Error in updating user {}", ex);
            throw new DatabaseException("Error in updating user " + ex);
        } finally {
            closeEntityManager();
        }
    }

    /**
     * 
     * @param user
     *            = Deletes the user in the user table.
     */
    public void deleteUser(User user) throws DatabaseException {
        LOG.debug("Deleting user table {}", user.getId());
        User toBeDeletedUser = new User();
        try {
            em = getEntityManager();
            toBeDeletedUser = em.getReference(User.class, user.getId());
            if (toBeDeletedUser != null) {
                em.getTransaction().begin();
                em.remove(toBeDeletedUser);
                em.getTransaction().commit();
            }
        } catch (Exception ex) {
            LOG.error("Error in deleting user {}", ex);
            throw new DatabaseException("Error in deleting user " + ex);
        } finally {
            closeEntityManager();
        }
    }

    /**
     * 
     * @return All users in the database
     * @throws DatabaseException
     */
    public List<User> getAllUsers() throws DatabaseException {
        LOG.debug("Get all users");
        List<User> users = null;
        try {
            em = getEntityManager();
            TypedQuery<User> getAllUsersFromTable = em.createNamedQuery(
                    DatabaseConstants.FIND_ALL_USERS, User.class);
            users = getAllUsersFromTable.getResultList();
        } catch (Exception ex) {
            LOG.error("Error in getting users {}", ex);
            throw new DatabaseException("Error in getting user " + ex);
        } finally {
            closeEntityManager();
        }
        return users;
    }
}
