package com.rakuten.online.test.persistence.manager;

import java.util.List;

import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rakuten.online.test.commons.util.DatabaseConstants;
import com.rakuten.online.test.commons.util.DatabaseException;
import com.rakuten.online.test.persistence.Question;

public class QuestionDAOImpl extends AbstractEntityManager {

	private static final Logger LOG = LoggerFactory
			.getLogger(UserInfoDAOImpl.class);

	public void addQuestion(Question ques) throws DatabaseException {
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			em.persist(ques);
			em.getTransaction().commit();
			em.getEntityManagerFactory().getCache().evictAll();
		} catch (Exception ex) {
			LOG.error("Error in adding user to user table {}", ex);
			throw new DatabaseException("Error in adding user to user table "
					+ ex);
		} finally {
			closeEntityManager();
		}

	}

	public void updateQuestion(Question ques) throws DatabaseException {
		Question updatingQuestion = new Question();
		try {
			em = getEntityManager();
			updatingQuestion = em.find(Question.class, ques.getQId());
			if (updatingQuestion != null) {
				em.getTransaction().begin();
				updatingQuestion.setCategory(ques.getCategory());
				updatingQuestion
						.setCorrectAnswerIds(ques.getCorrectAnswerIds());
				updatingQuestion.setQuestion(ques.getQuestion());
				updatingQuestion.setWeightage(ques.getWeightage());
				updatingQuestion.setAnswers(ques.getAnswers());
				em.getTransaction().commit();
			}
		} catch (Exception ex) {
			LOG.error("Error in updating user {}", ex);
			throw new DatabaseException("Error in updating user " + ex);
		} finally {
			closeEntityManager();
		}
	}

	public void deleteQuestion(Question ques) throws DatabaseException {
		Question questionToBeDeleted = new Question();
		try {
			em = getEntityManager();
			questionToBeDeleted = em.getReference(Question.class, ques.getQId());
			em.getTransaction().begin();
			em.remove(questionToBeDeleted);
			em.getTransaction().commit();
		} catch (Exception ex) {
			LOG.error("Error in updating user {}", ex);
			throw new DatabaseException("Error in updating user " + ex);
		} finally {
			closeEntityManager();
		}
	}

	public List<Question> getAllQuestions() throws DatabaseException {
		List<Question> questions = null;
		try {
			em = getEntityManager();
			TypedQuery<Question> getAllQuestionsQuery = em.createNamedQuery(
					DatabaseConstants.FIND_ALL_QUESTIONS, Question.class);
			questions = getAllQuestionsQuery.getResultList();
		} catch (Exception ex) {
			LOG.error("Error in getting all questions {}", ex);
			throw new DatabaseException("Error in getting question " + ex);
		} finally {
			closeEntityManager();
		}
		return questions;
	}

	public Question getQuestion(int questionId) throws DatabaseException {
		LOG.debug("Getting question for questionId {}", questionId);
		Question question = null;
		try {
			em = getEntityManager();
			question = em.find(Question.class, questionId);
		} catch (Exception ex) {
			LOG.error("Error in finding question {}", ex);
			throw new DatabaseException("Error in finding question " + ex);
		} finally {
			closeEntityManager();
		}
		return question;
	}
}