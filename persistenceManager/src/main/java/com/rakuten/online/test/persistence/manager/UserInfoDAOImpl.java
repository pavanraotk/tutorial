package com.rakuten.online.test.persistence.manager;

import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rakuten.online.test.commons.util.DatabaseConstants;
import com.rakuten.online.test.commons.util.DatabaseException;
import com.rakuten.online.test.persistence.User;
import com.rakuten.online.test.persistence.UserInfo;

public class UserInfoDAOImpl extends AbstractEntityManager {

    private static final Logger LOG = LoggerFactory
            .getLogger(UserInfoDAOImpl.class);

    /**
     * @param userInfo
     * @throws DatabaseException
     * 
     *             Adds user info to the user info table, throws database
     *             exception in case of exception
     */
    public void addUserInfo(UserInfo userInfo) throws DatabaseException {
        LOG.debug("Adding user info to table");
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(userInfo);
            em.getTransaction().commit();
            em.getEntityManagerFactory().getCache().evictAll();
        } catch (Exception ex) {
            LOG.error("Error in adding user info {}", ex);
            throw new DatabaseException("Error in adding user info " + ex);
        } finally {
            closeEntityManager();
        }

    }

    /**
     * 
     * @param user
     * @return userInfo
     * 
     *         Returns userInfo object for given user from the table, throws
     *         database exception.
     */
    public UserInfo getUser(User user) throws DatabaseException {
        UserInfo userInfo = null;
        LOG.debug("Getting user info for user {}", user.getName());
        try {
            em = getEntityManager();
            TypedQuery<UserInfo> getUserInfoForUser = em.createNamedQuery(
                    DatabaseConstants.FIND_USERINFO_FROM_USER, UserInfo.class);
            getUserInfoForUser.setParameter("user", user);
            userInfo = getUserInfoForUser.getSingleResult();
        } catch (Exception ex) {
            LOG.error(
                    "Error in getting user info for user {} with exception {}",
                    user.getName(), ex);
            throw new DatabaseException("Error in getting user info for user "
                    + user.getName() + DatabaseConstants.WITH_EXCEPTION + ex);
        } finally {
            closeEntityManager();
        }
        return userInfo;
    }

    /**
     * 
     * @param userInfo
     * @throws DatabaseException
     *             Updating user info.
     */
    public void updateUserInfo(UserInfo userInfo) throws DatabaseException {
        UserInfo userInfoToBeUpdated = null;
        LOG.debug("Updating user info");
        try {
            em = getEntityManager();
            userInfoToBeUpdated = em.find(UserInfo.class, userInfo.getGuid());
            if (userInfoToBeUpdated != null) {
                em.getTransaction().begin();
                userInfoToBeUpdated.setExperience(userInfo.getExperience());
                userInfoToBeUpdated.setMail(userInfo.getMail());
                userInfoToBeUpdated.setPhone(userInfo.getPhone());
                userInfoToBeUpdated.setUser(userInfo.getUser());
                em.getTransaction().commit();
            }
        } catch (Exception ex) {
            LOG.error(
                    "Error in udpating user info for user {} with exception {}",
                    userInfo.getUser().getName(), ex);
            throw new DatabaseException("Error in updating user info for user "
                    + userInfo.getUser().getName()
                    + DatabaseConstants.WITH_EXCEPTION + ex);
        } finally {
            closeEntityManager();
        }

    }

    /**
     * 
     * @param userInfo
     * @throws DatabaseException
     * 
     *             Deleting user info from table.
     */
    public void deleteUser(UserInfo userInfo) throws DatabaseException {
        LOG.debug("Deleting user info for user {}", userInfo.getUser()
                .getName());
        UserInfo userInfoToBeDeleted = null;
        try {
            em = getEntityManager();
            userInfoToBeDeleted = em.getReference(UserInfo.class,
                    userInfo.getGuid());
            if (userInfoToBeDeleted != null) {
                em.getTransaction().begin();
                em.remove(userInfoToBeDeleted);
                em.getTransaction().commit();
            }
        } catch (Exception ex) {
            LOG.error(
                    "Error in deleting user info for user {} with exception {}",
                    userInfo.getUser().getName(), ex);
            throw new DatabaseException("Error in deleting user info for user "
                    + userInfo.getUser().getName()
                    + DatabaseConstants.WITH_EXCEPTION + ex);
        } finally {
            closeEntityManager();
        }
    }

    public UserInfo getUserFromEmail(String emailId) throws DatabaseException {
        LOG.debug("Getting user info for email ID {}", emailId);
        UserInfo userInfo = null;
        try {
            em = getEntityManager();
            TypedQuery<UserInfo> getUserInfoFromEmail = em.createNamedQuery(
                    DatabaseConstants.FIND_USERINFO_FROM_EMAILID,
                    UserInfo.class);
            getUserInfoFromEmail.setParameter("mail", emailId);
            userInfo = getUserInfoFromEmail.getSingleResult();
        } catch (Exception ex) {
            LOG.error(
                    "Error in getting user info for mail {} with exception {}",
                    emailId, ex);
            throw new DatabaseException("Error in getting user info for mail "
                    + emailId + DatabaseConstants.WITH_EXCEPTION + ex);
        } finally {
            closeEntityManager();
        }
        return userInfo;
    }
}
