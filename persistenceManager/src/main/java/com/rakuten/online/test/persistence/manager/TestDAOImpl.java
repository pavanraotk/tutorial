package com.rakuten.online.test.persistence.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rakuten.online.test.commons.util.DatabaseConstants;
import com.rakuten.online.test.commons.util.DatabaseException;
import com.rakuten.online.test.persistence.Answer;
import com.rakuten.online.test.persistence.Question;
import com.rakuten.online.test.persistence.Test;

public class TestDAOImpl extends AbstractEntityManager {

	private static final Logger LOG = LoggerFactory
			.getLogger(TestDAOImpl.class);

	/**
	 * 
	 * @param test
	 *            Test object is added to the Test table. Unique constraint
	 *            violation is caught in the exception
	 */

	public void addTest(Test test) throws DatabaseException {
		LOG.debug("Adding test to the test table.");
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			em.persist(test);
			List<Question> questionList = new ArrayList<Question>();
			questionList.addAll(test.getQuestions());
			List<Answer> answerList = new ArrayList<Answer>();
			for (Question q : questionList) {
				em.persist(q);
				answerList.addAll(q.getAnswers());
				for (Answer a : answerList) {
					em.persist(a);
				}
			}
			em.getTransaction().commit();
			em.getEntityManagerFactory().getCache().evictAll();
		} catch (Exception ex) {
			LOG.error("Error in adding test to test table {}", ex);
			throw new DatabaseException("Error in adding test to test table "
					+ ex);
		} finally {
			closeEntityManager();
		}
	}

	/**
	 * 
	 * @param testId
	 * @return user: Returns the user object for given ID.
	 */
	public Test getTest(int testId) throws DatabaseException {
		LOG.debug("Getting test for testId {}", testId);
		Test test = null;
		try {
			em = getEntityManager();
			test = em.find(Test.class, testId);
		} catch (Exception ex) {
			LOG.error("Error in finding test {}", ex);
			throw new DatabaseException("Error in finding test " + ex);
		} finally {
			closeEntityManager();
		}
		return test;
	}

	/**
	 * 
	 * @param test
	 *            Updates the test row in the table.
	 */
	public void updateTest(Test test) throws DatabaseException {
		LOG.debug("Update test table {}", test.getTestId());
		Test updatingTest = new Test();
		try {
			em = getEntityManager();
			updatingTest = em.find(Test.class, test.getTestId());
			if (updatingTest != null) {
				em.getTransaction().begin();
				updatingTest.setCategory(test.getCategory());
				updatingTest.setDuration(test.getDuration());
				updatingTest.setExperience(test.getExperience());
				updatingTest.setQuestions(test.getQuestions());
				em.getTransaction().commit();
			}
		} catch (Exception ex) {
			LOG.error("Error in updating test {}", ex);
			throw new DatabaseException("Error in updating test " + ex);
		} finally {
			closeEntityManager();
		}
	}

	/**
	 * 
	 * @param test
	 *            = Deletes the test in the test table.
	 */
	public void deleteTest(Test test) throws DatabaseException {
		LOG.debug("Deleting the test from test table {} for testId ",
				test.getTestId());
		Test toBeDeletedTest = new Test();
		try {
			em = getEntityManager();
			toBeDeletedTest = em.getReference(Test.class, test.getTestId());
			em.getTransaction().begin();
			if (toBeDeletedTest != null)
				em.remove(toBeDeletedTest);
			em.getTransaction().commit();
		} catch (NullPointerException ex) {
			LOG.error("No test found {}", ex);
			throw new DatabaseException("No test found " + ex);
		} catch (Exception ex) {
			LOG.error("Error in deleting test {}", ex);
			throw new DatabaseException("Error in deleting test " + ex);
		} finally {
			closeEntityManager();
		}
	}

	public List<Test> getAllTest() throws DatabaseException {
		LOG.debug("Get All Test");
		List<Test> allTest = null;
		try {
			em = getEntityManager();
			Query allTestQuery = em.createNamedQuery(
					DatabaseConstants.FIND_ALL_TESTS, Test.class);
			allTest = allTestQuery.getResultList();
			em.getTransaction().commit();
		} catch (Exception e) {
			LOG.error("Error in getAllTest method");
			throw new DatabaseException("Error in getAllTest method" + e);
		} finally {
			closeEntityManager();
		}
		return allTest;
	}

	public List<Test> getTestByCategory(int category) throws DatabaseException {
		LOG.debug("Get Test by Category");
		List<Test> allTest = null;
		try {
			em = getEntityManager();
			Query allTestQuery = em.createNamedQuery(
					DatabaseConstants.FIND_ALL_TESTS_BY_CATEGORY, Test.class);
			allTestQuery.setParameter("category", category);
			allTest = allTestQuery.getResultList();
		} catch (Exception e) {
			LOG.error("Error in getTestByCategory method {}", e);
			throw new DatabaseException("Error in getTestBy Category method"
					+ e);
		} finally {
			closeEntityManager();
		}
		return allTest;
	}

	public List<Test> getTestByExperience(BigDecimal experience)
			throws DatabaseException {
		LOG.debug("Get getTestByExperience method");
		List<Test> allTest = null;
		try {
			em = getEntityManager();
			Query allTestQuery = em.createNamedQuery(
					DatabaseConstants.FIND_ALL_TESTS_BY_EXPERIENCE, Test.class);
			allTestQuery.setParameter("experience", experience);
			allTest = allTestQuery.getResultList();
		} catch (Exception e) {
			LOG.error("Error in getTestByExperience method");
			throw new DatabaseException("Error in getTestByExperience method"
					+ e);
		} finally {
			closeEntityManager();
		}
		return allTest;
	}

}
